/**
 * @file CTPfragment/Issue.h
 * @author <a href="mailto:berge@cern.ch">David Berge</a>
 *
 * $Author$
 * $Revision$
 * $Date$
 * @brief Defines the CTPfragment Issue.
 */

#ifndef CTP_FRAGMENT_ISSUE_H
#define CTP_FRAGMENT_ISSUE_H

#include "ers/Issue.h"

/**
 * Macro to facilitate hexadecimal conversion
 *
 * @param x The number that will be printed out as hexdecimal
 */
#define CTPFRAGMENT_HEX(x) std::hex << "0x" << x << std::dec

/**
 * Declares the base Issue
 */
ERS_DECLARE_ISSUE(CTPfragment, Issue, "CTPfragment base issue",)

/**
 * Generic CTPfragment issue
 */
ERS_DECLARE_ISSUE_BASE(CTPfragment, GenericIssue, CTPfragment::Issue, 
		       "Found a problem: " << what << ": ", ,((const char*)what))

/**
 * Simplifies the use of this Issue
 *
 * @param what Description of the problem
 */
#define CTPFRAGMENT_GENERIC_ISSUE(what) \
  CTPfragment::GenericIssue(ERS_HERE,what)

/**
 * This exception is supposed to be thrown when NULL pointers are passed in
 */
ERS_DECLARE_ISSUE_BASE(CTPfragment, NullFragmentPointer, CTPfragment::Issue, 
		       "NULL pointer to " << fragment << " received: ", ,((const char*)fragment))

/**
 * Simplifies the use of this Issue
 *
 * @param fragment Fragment type that was passed in
 */
#define CTPFRAGMENT_NULL_POINTER(fragment) \
  CTPfragment::NullFragmentPointer(ERS_HERE, fragment)


/**
 * Base issue for inconsistent / invalid fragments
 */
ERS_DECLARE_ISSUE_BASE(CTPfragment, InconsistencyBase, CTPfragment::Issue, 
		       "-> Invalid " << where << ": ",
		       ,((const char*)where))
/**
 * This exception is supposed to be thrown whenever a fragment field
 * does not contain the expected value
 */

ERS_DECLARE_ISSUE_BASE(CTPfragment,Inconsistency, CTPfragment::InconsistencyBase,
		       "Found " << CTPFRAGMENT_HEX(found) << " instead of " 
		       << CTPFRAGMENT_HEX(expected) << " ",((const char*)where),
		       ((uint32_t)found) ((uint32_t)expected))

/**
 * Simplifies the usage of this Issue
 *
 * @param where Description of the fragment field that showed an
 * unexpected value
 * @param found Value found found in the fragment
 * @param expected Value expected in the fragment
 *
 */
#define CTPFRAGMENT_INCONSISTENCY(where,found,expected) \
  CTPfragment::Inconsistency(ERS_HERE, where, found, expected)

/**
 * This exception is supposed to be thrown when the CTP data fragment
 * is empty and thus does not contain the time stamp
 */
ERS_DECLARE_ISSUE_BASE(CTPfragment, TimeOutOfRange, CTPfragment::Issue, 
		       "ROD data too short, time stamp not available - ", ,)

/**
 * Simplifies the usage of this Issue
 *
 */
#define CTPFRAGMENT_NO_TIME_STAMP() \
  CTPfragment::TimeOutOfRange(ERS_HERE)

/**
 * This exception is supposed to be thrown when the CTP data fragment
 * is shorter than expected
 */
ERS_DECLARE_ISSUE_BASE(CTPfragment, TriggerWordsOutOfRange, CTPfragment::Issue, 
		       "ROD data too short, trigger decision not available - ", ,)

/**
 * Simplifies the usage of this Issue
 *
 */
#define CTPFRAGMENT_NO_TRIGGER_WORDS() \
  CTPfragment::TriggerWordsOutOfRange(ERS_HERE)

#endif
