/**
 * @file CTPfragment/CTPdataformat.h
 * @author <a href="mailto:berge@cern.ch">David Berge</a>
 *
 * $Author$
 * $Revision$
 * 
 * @brief Defines CTP data format.
 */

#ifndef CTP_DATAFORMAT_H
#define CTP_DATAFORMAT_H

#include <inttypes.h>

namespace CTPdataformat {

  /** 
   *  Number of time words that appear only once per fragment and
   *  specify the time of the actual trigger that caused a L1A
   */
  static const uint32_t NumberTimeWords = 2; 
  static const uint32_t PITpos = NumberTimeWords;

  static const uint32_t PITwords_v0         = 6; // 160 PITs = 5 x 32 bit words + 32 bits BCID etc.
  static const uint32_t AuxPITwordPos_v0    = 6; // PIT word with BCID, prescaled clock and random trigger
  static const uint32_t TBPwords_v0         = 8; // 256 trigger items = 8 x 32 bit words
  static const uint32_t TAPwords_v0         = TBPwords_v0; // 256 trigger items = 8 x 32 bit words
  static const uint32_t TAVwords_v0         = TBPwords_v0; // 256 trigger items = 8 x 32 bit words
  static const uint32_t DAQwordsPerBunch_v0 = PITwords_v0 + TBPwords_v0 + TAPwords_v0 + TAVwords_v0;
  static const uint32_t TBPpos_v0           = PITpos+PITwords_v0;
  static const uint32_t TAPpos_v0           = TBPpos_v0+TAPwords_v0;
  static const uint32_t TAVpos_v0           = TAPpos_v0+TAPwords_v0;

  static const uint32_t PITwords_v4         = 17; // 320 TIPs = 16 x 32 bit words + 32 bits BCID etc.
  static const uint32_t AuxPITwordPos_v4    = 17; // PIT word with BCID, prescaled clock and random trigger
  static const uint32_t TBPwords_v4         = 16; // 512 trigger items = 16 x 32 bit words
  static const uint32_t TAPwords_v4         = TBPwords_v4; // 512 trigger items = 16 x 32 bit words
  static const uint32_t TAVwords_v4         = TBPwords_v4; // 512 trigger items = 16 x 32 bit words
  static const uint32_t DAQwordsPerBunch_v4 = PITwords_v4 + TBPwords_v4 + TAPwords_v4 + TAVwords_v4;
  static const uint32_t TBPpos_v4           = PITpos+PITwords_v4;
  static const uint32_t TAPpos_v4           = TBPpos_v4+TBPwords_v4;
  static const uint32_t TAVpos_v4           = TAPpos_v4+TAPwords_v4;

  static const uint32_t TBPpos_v5           = PITpos;
  static const uint32_t TAVpos_v5           = TBPpos_v5+TBPwords_v4;
  static const uint32_t LVL2wordsPerBunch_v5 = TBPwords_v4 + TAVwords_v4;

  static const uint32_t TimeNanosecondsOffset  = 4; // nanoseconds are the 28 upper bits
  static const uint32_t TimeNanosecondsTicks   = 5; // counted in units of 5 nanosec ticks

  static const uint32_t BcidMask_v0            = 0xfff; // 12 bits for BCID (in early data)
  static const uint32_t BcidMask_v1            = 0xf;   // 4 lowest bits for BCID
  static const uint32_t BcidMask_v4            = 0xfff; // 12 bits for BCID
  static const uint32_t BcidShift_v0           = 0;
  static const uint32_t BunchGroupMask_v0      = 0x0;  // 0-bit word with bunch group information
  static const uint32_t BunchGroupMask_v1      = 0xff; // 8-bit word with bunch group information
  static const uint32_t BunchGroupMask_v4      = 0xffff; // 8-bit word with bunch group information
  static const uint32_t BunchGroupShift_v0     = 0; // did not exist in version 0
  static const uint32_t BunchGroupShift_v1     = 4;
  static const uint32_t BunchGroupShift_v4     = 16;
  static const uint32_t RandomTrigMask_v0      = 0x3; // 2 bits for 2 random triggers
  static const uint32_t RandomTrigMask_v4      = 0xf; // 4 bits for 4 random triggers
  static const uint32_t RandomTrigShift_v0     = 12;
  static const uint32_t PrescaledClockMask_v0  = 0x3; // 2 bits for 2 prescaled-clock triggers
  static const uint32_t PrescaledClockMask_v4  = 0x0; // 0 bits for 0 prescaled-clock triggers
  static const uint32_t PrescaledClockShift_v0 = 14;
  static const uint32_t PrescaledClockShift_v4 = 0;   // does not exist in version 4
  static const uint32_t LumiBlockMask          = 0xffff; // 16 lowest bits of det. event type
  static const uint32_t LumiBlockShift         = 0;

  static const uint32_t L1APositionMask     = 0x3f; // 6 bits are reserved for L1A position

  // ***************************************************************
  // v0: before moving the L1A position to the eformat-version field
  static const uint32_t L1APositionShift_v0    = 16; // bits 16 to 21 are L1A position
  static const uint32_t DetEvtTypeExtraBitsMask_v0 = 0x3ff; // 10 remaining bits of det. event type
  static const uint32_t DetEvtTypeExtraBitsShift_v0 = 22;

  // the following constants could be used in uint32_t CTPfragment::hltCounter(const uint32_t* rod):
  static const uint32_t HltCounterMask_v0 = DetEvtTypeExtraBitsMask_v0; // 10 remaining bits of det. event type
  static const uint32_t HltCounterShift_v0 = DetEvtTypeExtraBitsShift_v0;
  // end v0
  // ***************************************************************

  // ***************************************************************
  // v1: after moving the L1A position to the eformat-version field to free bits for the HLT counter
  static const uint32_t L1APositionShift_v1    = 8; // bits 8 to 13 are L1A position
  static const uint32_t DetEvtTypeExtraBitsMask_v1 = 0xffff; // 16 remaining bits of det. event type
  static const uint32_t DetEvtTypeExtraBitsShift_v1 = 16;

  // the following constants could be used in uint32_t CTPfragment::hltCounter(const uint32_t* rod):
  static const uint32_t HltCounterMask_v1 = DetEvtTypeExtraBitsMask_v1; // 16 remaining bits of det. event type
  static const uint32_t HltCounterShift_v1 = DetEvtTypeExtraBitsShift_v1;
  // end v1
  // ***************************************************************

  // ***************************************************************
  // v2: First version of CTP data with programmable extra words (up to 50), and time difference in BC to previous L1A
  // in v2 the L1A position is stored in bits 4 to 9
  static const uint32_t L1APositionShift_v2    = 4;

  static const uint32_t ProgrammableExtraWordsShift_v2 = 10; // bits 10 to 15 are the number of additional programmable words
  static const uint32_t ProgrammableExtraWordsMask_v2 = 0x3f; // 6 bits for the number of additional programmable words
  // end v2
  // ***************************************************************

  static const uint32_t CTPFormatVersionMask = 0xf; // lower 4 bit of format minor version field are CTP version
  static const uint32_t CTPFormatVersionShift = 0;

  static const uint32_t TimeNanosecondsPos = 0;
  static const uint32_t TimeSecondsPos = 1;


  namespace Helper {
    /**
     * now a few constants that concern the eformat, needed to ease
     * dealing with CTP ROD fragments.
     */
    static const uint32_t HeaderMarkerPos = 0;
    static const uint32_t HeaderSizePos = 1;
    static const uint32_t FormatVersionPos = 2;
    static const uint32_t SourceIdPos = 3;
    static const uint32_t DetectorTypePos = 8;

    /**
     * Trigger-type bits
     */
    static const uint32_t TTPhysicsCalibrationBit = 7;
    static const uint32_t TTCalibrationDetectorCodeShift = 3;
    static const uint32_t TTCalibrationDetectorCodeMask = 0xf;
  }
}

#endif // CTP_DATAFORMAT_H
