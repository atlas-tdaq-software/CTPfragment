/**
 * @file CTPfragment/CTPfragment.h
 * @author <a href="mailto:berge@cern.ch">David Berge</a>
 *
 * $Author$
 * $Revision$
 * 
 * @brief Provides access to the LumiBlockNumber and the BC GPS time from the CTP ROD fragment.
 */

#ifndef CTP_FRAGMENT_H
#define CTP_FRAGMENT_H

#include <inttypes.h>
#include <vector>
#include "eformat/eformat.h"

namespace CTPfragment {
  /**
   * Obtain the lumi block number from a CTP ROD fragment
   *
   * @param rod Pointer to start of ROD fragment
   */
  uint32_t lumiBlockNumber(const uint32_t* rod);
  /**
   * Obtain the lumi block number from a CTP ROB fragment
   *
   * @param rob Pointer to a ROB fragment
   */
  uint32_t lumiBlockNumber(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the L1A bunch position from a CTP ROD fragment
   *
   * @param rod Pointer to start of ROD fragment
   */
  uint32_t lvl1AcceptBunch(const uint32_t* rod);
  /**
   * Obtain the L1A bunch position from a CTP ROB fragment
   *
   * @param rob Pointer to a ROB fragment
   */
  uint32_t lvl1AcceptBunch(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the extra bits of the detector event-type word from a CTP ROD fragment
   *
   * @param rod Pointer to start of ROD fragment
   */
  uint32_t detEvtTypeExtraBits(const uint32_t* rod);
  /**
   * Obtain the extra bits of the detector event-type word from a CTP ROB fragment
   *
   * @param rob Pointer to a ROB fragment
   */
  uint32_t detEvtTypeExtraBits(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the HLT special counter from a CTP ROD fragment
   *
   * @param rod Pointer to start of ROD fragment
   */
  uint32_t hltCounter(const uint32_t* rod);
  /**
   * Obtain the HLT special counter from a CTP ROD fragment
   *
   * @param rob Pointer to a ROB fragment
   */
  uint32_t hltCounter(const eformat::ROBFragment<const uint32_t*>* rob);

  /**
   * Get the CTP data format version from ROD fragment
   *
   * @param rod Pointer to start of ROD fragment
   */
  uint32_t ctpFormatVersion(const uint32_t* rod);
  /**
   * Get the CTP data format version from ROB fragment
   *
   * @param rob Pointer to a ROB fragment
   */
  uint32_t ctpFormatVersion(const eformat::ROBFragment<const uint32_t*>* rob);

  /**
   * Obtain the number of extra payload words
   *
   * @param rod Pointer to start of ROD fragment
   */
  uint32_t numberExtraPayloadWords(const uint32_t* rod);
  /**
   * Obtain the number of extra payload words
   *
   * @param rob Pointer to a ROB fragment
   */
  uint32_t numberExtraPayloadWords(const eformat::ROBFragment<const uint32_t*>* rob);

  /**
   * Obtain the GPS time stamp from a CTP ROD fragment. The time is in
   * seconds since 1 January 1970 GMT.
   *
   * @param rod Pointer to start of ROD fragment
   * @param sec Variable to retrieve the GPS seconds
   * @param nsec Variable to retrieve the GPS nano-seconds
   */
  void time(const uint32_t* rod,uint32_t& sec, uint32_t& nsec);
  /**
   * Obtain the GPS time stamp from a CTP ROB fragment. The time is in
   * seconds since 1 January 1970 GMT.
   *
   * @param rob Pointer to ROB fragment
   * @param sec Variable to retrieve the GPS seconds
   * @param nsec Variable to retrieve the GPS nano-seconds
   */
  void time(const eformat::ROBFragment<const uint32_t*>* rob,uint32_t& sec, uint32_t& nsec);
  /**
   * Obtain the final LVL1 trigger decision from the CTP payload
   * data. The caller obtains a vector with eight uint32_t's, the
   * trigger decision from the CTP data.
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> triggerDecisionAfterVeto(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the LVL1 trigger decision after prescales, before veto
   * (deadtime, busy, etc.), from the CTP payload data. The caller
   * obtains a vector with uint32_t's, the trigger decision
   * before veto from the CTP data.
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> triggerDecisionAfterPrescales(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the LVL1 trigger decision before prescales are applied
   * from the CTP payload data. The caller obtains a vector with
   * uint32_t's, the trigger decision before prescales from the CTP
   * data.
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> triggerDecisionBeforePrescales(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the LVL1 trigger decision from the CTP payload data. The
   * caller obtains a vector with uint32_t's, the trigger
   * decision from the CTP data.
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> triggerDecision(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the LVL1 trigger inputs to the CTP (so called PIT, or TIP for the CTP+)
   * from the CTP payload data. The caller obtains a vector with 
   * uint32_t's, the PIT/TIP pattern, i.e. the trigger inputs, from the CTP
   * data.
   *
   * @param rob Pointer to ROB fragment
   */
  uint32_t patternInTimeAuxWord(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the last word from the  LVL1 trigger inputs to the CTP (so called PIT/TIP)
   * from the CTP payload data. The caller obtains a uint32_t including information about
   * the bunch crossing and random/prescaled triggers from the CTP data.
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> patternInTimeTriggerInputs(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the final LVL1 trigger decision from the CTP payload
   * data. The caller obtains a vector with uint32_t's, the
   * trigger decision from the CTP data for a given BC.
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> triggerDecisionAfterVeto(const eformat::ROBFragment<const uint32_t*>* rob, uint32_t bunchCrossing);
  /**
   * Obtain the LVL1 trigger decision after prescales, before veto
   * (deadtime, busy, etc.), from the CTP payload data. The caller
   * obtains a vector with uint32_t's, the trigger decision
   * before veto from the CTP data for a given BC.
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> triggerDecisionAfterPrescales(const eformat::ROBFragment<const uint32_t*>* rob, uint32_t bunchCrossing);
  /**
   * Obtain the LVL1 trigger decision before prescales are applied
   * from the CTP payload data. The caller obtains a vector with
   * uint32_t's, the trigger decision before prescales from the CTP
   * data for a given BC.
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> triggerDecisionBeforePrescales(const eformat::ROBFragment<const uint32_t*>* rob, uint32_t bunchCrossing);
  /**
   * Obtain the LVL1 trigger decision from the CTP payload data. The
   * caller obtains a vector with eight uint32_t's, the trigger
   * decision from the CTP data for a given BC.
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> triggerDecision(const eformat::ROBFragment<const uint32_t*>* rob, uint32_t bunchCrossing);
  /**
   * Obtain the LVL1 trigger inputs to the CTP (so called PIT)
   * from the CTP payload data. The caller obtains a vector with 
   * uint32_t's, the PIT pattern, i.e. the trigger inputs, from the CTP
   * data for a given BC.
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> patternInTimeTriggerInputs(const eformat::ROBFragment<const uint32_t*>* rob, uint32_t bunchCrossing);
  /**
   * Obtain the CTP PIT BCID. The caller obtains a uint16_t, which
   * is the 4-bit BCID for <=v3 and the 12-bit BCID for v4
   * stored in the payload data of the CTP.
   *
   * @param rob Pointer to ROB fragment
   */
  uint16_t bcid(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the CTP Bunch Group Word. The caller obtains a uint16_t, which
   * is the BG trigger pattern stored in the payload data of the CTP.
   *
   * @param rob Pointer to ROB fragment
   */
  uint16_t bunchGroup(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the random triggers. The caller obtains a vector of
   * bools, which correspond to the random triggers as they are stored
   * in the payload data of the CTP. 'true' means random trigger has fired.
   * Note that for <=v3 there are 2 random triggers, for v4 there are 4
   * 
   * @param rob Pointer to ROB fragment
   */
  std::vector<bool> randomTriggers(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Obtain the prescaled clock triggers. The caller obtains a vector of
   * bools, which correspond to prescaled clock trigger 1 and 2, as they are
   * stored in the payload data of the CTP. 'true' means prescaled clock
   * trigger 1/2 has fired.
   * Note that for <=v3 there are 2 prescaled clock triggers, for v4 there are 0
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<bool> prescaledClockTriggers(const eformat::ROBFragment<const uint32_t*>* rob);
  /**
   * Build vector of StreamTag objects based on LVL1 trigger type
   *
   * @param l1tt 8-bit LVL1 trigger type
   * @param streamtags vector for StreamTag objects
   */
  void streamTags(const uint8_t& l1tt, std::vector<eformat::helper::StreamTag> & streamtags);
  std::vector<uint32_t> getAllWords(const eformat::ROBFragment<const uint32_t*>* rob);

  /**
   * Get programmable extra words from the payload data
   *
   * @param rob Pointer to ROB fragment
   */
  std::vector<uint32_t> extraPayloadWords(const eformat::ROBFragment<const uint32_t*>* rob);

  /**
   * Get time in BC since previous L1A
   *
   * @param rob Pointer to ROB fragment
   */
  uint32_t bunchCrossingsSincePreviousL1A(const eformat::ROBFragment<const uint32_t*>* rob);

  /**
   * Get turn counter
   *
   * @param rob Pointer to ROB fragment
   */
  uint32_t turnCounter(const eformat::ROBFragment<const uint32_t*>* rob);
}

#endif // CTP_FRAGMENT_H

