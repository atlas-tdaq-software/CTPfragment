#include <string>
#include <vector>
#include <sstream>

#include "ers/ers.h"
#include "CTPfragment/CTPfragment.h"
#include "CTPfragment/Issue.h"
#include "CTPfragment/CTPdataformat.h"

//
// private helper section
// =================================
//
void checkROD(const uint32_t* rod);
void checkROB(const eformat::ROBFragment<const uint32_t*>* rob);
enum WordType {PIT, PITAUX, TBP, TAP, TAV};
typedef enum WordType WordType;
std::vector<uint32_t> getTriggerWords(WordType type,
				      const eformat::ROBFragment<const uint32_t*>* rob);
std::vector<uint32_t> getTriggerWords(WordType type,
				      const eformat::ROBFragment<const uint32_t*>* rob,
				      uint32_t bunchCrossing);
// =================================

uint32_t CTPfragment::bunchCrossingsSincePreviousL1A(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get L1A deltaT from ROB Fragment");
  const uint32_t* rod;
  rob->rod_start(rod);
  checkROD(rod);

  if(ctpFormatVersion(rod) < 2) {
    // format version smaller than 2 means this information wasn't yet
    // in the data
    return 0;
  } else {

    uint32_t numberAdditionalWords = numberExtraPayloadWords(rod);
    // startIndex is the fragment size, hence points to one after the
    // final word
    uint32_t startIndex = rob->rod_fragment_size_word(); 
    // correct for trailer
    startIndex -= rob->rod_trailer_size_word();
    // correct for status words
    startIndex -= rob->rod_nstatus();
    // now subtract the additional words
    startIndex -= numberAdditionalWords;
    // startIndex is now exactly the size of the trigger-bit part of
    // the payload data. hence it points to the first of the extra
    // words, which is the time since the previous crossing

    // debug
    if(false) {
      std::cout << "Payload size " << rob->rod_fragment_size_word() << std::endl;
      std::cout << "Additional words " << numberAdditionalWords << std::endl;
      std::cout << "Start index of time since previous L1A " << startIndex << std::endl; 
    }

    // Now return the word with the number of crossings since the
    // previous L1A
    return rod[startIndex];
  }
}

uint32_t CTPfragment::turnCounter(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get turn counter from ROB Fragment");
  const uint32_t* rod;
  rob->rod_start(rod);
  checkROD(rod);

  if(ctpFormatVersion(rod) < 3) {
    // format version smaller than 3 means this information wasn't yet
    // in the data
    return 0;
  } else {

    uint32_t numberAdditionalWords = numberExtraPayloadWords(rod);
    // startIndex is the fragment size, hence points to one after the
    // final word
    uint32_t startIndex = rob->rod_fragment_size_word(); 
    // correct for trailer
    startIndex -= rob->rod_trailer_size_word();
    // correct for status words
    startIndex -= rob->rod_nstatus();
    // now subtract the additional words
    startIndex -= numberAdditionalWords;
    // startIndex is now exactly the size of the trigger-bit part of
    // the payload data. hence it points to the first of the extra
    // words, which is the time since the previous crossing, so need to
    // add 1 to get to the turn counter
    startIndex += 1;

    // debug
    if(false) {
      std::cout << "Payload size " << rob->rod_fragment_size_word() << std::endl;
      std::cout << "Additional words " << numberAdditionalWords << std::endl;
      std::cout << "Start index of time since previous L1A " << startIndex << std::endl; 
    }

    // Now return the word with the number of crossings since the
    // previous L1A
    return rod[startIndex];
  }
}

std::vector<uint32_t> CTPfragment::extraPayloadWords(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get extra payload words from ROB Fragment");
  const uint32_t* rod;
  rob->rod_start(rod);
  checkROD(rod);

  std::vector<uint32_t> vec;
  uint32_t formatV = ctpFormatVersion(rod);
  if(formatV < 2) {
    // format version smaller than 2 means this information wasn't yet
    // in the data
    return vec;
  } else {

    uint32_t numberExtraWords = numberExtraPayloadWords(rod);
    uint32_t numberAdditionalWords = numberExtraWords;
    // startIndex is the fragment size, hence points to one after the
    // final word
    uint32_t startIndex = rob->rod_fragment_size_word(); 
    // correct for trailer
    startIndex -= rob->rod_trailer_size_word();
    // correct for status words
    startIndex -= rob->rod_nstatus();
    // now subtract the additional words
    startIndex -= numberExtraWords;
    // startIndex is now exactly the size of the trigger-bit part of
    // the payload data. hence it points to the first of the extra
    // words, which is the time since the previous crossing; to get to
    // the true extra payload words for the HLT, add 2 to account for
    // the time-since-previous-crossing word, and the new turn counter
    // (in case format version number is at least 3)
    startIndex += 1;
    numberAdditionalWords-=1;
    if(formatV >= 3) {
      startIndex += 1;
      numberAdditionalWords-=1;
    }

    // debug:
    if(false) {
      std::cout << "Total size " << std::dec << rob->rod_fragment_size_word() << std::endl;
      std::cout << "Header size " << rob->rod_header_size_word() << std::endl;
      std::cout << "Trailer size " << rob->rod_trailer_size_word() << std::endl;
      std::cout << "Status words " << rob->rod_nstatus() << std::endl;
      std::cout << "Extra words " << numberExtraWords << std::endl;
      std::cout << "Additional words " << numberAdditionalWords << std::endl;
      std::cout << "Start index " << startIndex << std::endl; 
      std::cout << "Loop until index of " << (startIndex+numberAdditionalWords-1) << std::endl; 
    }

    // now fill vector with extra words and return it
    for(uint32_t i = startIndex; i < (startIndex+numberAdditionalWords); ++i) {
      vec.push_back(  rod[i] );
    }
    return vec;
  }
}

uint32_t CTPfragment::lumiBlockNumber(const uint32_t* rod)
{
  ERS_DEBUG(4,"Get LumiBlockNumber from ROD Fragment");
  checkROD(rod);
  return ((rod[CTPdataformat::Helper::DetectorTypePos] >> CTPdataformat::LumiBlockShift ) 
	  & CTPdataformat::LumiBlockMask);
}

uint32_t CTPfragment::lumiBlockNumber(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get LumiBlockNumber from ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  const uint32_t* rod;
  rob->rod_start(rod);
  return lumiBlockNumber(rod);
}

uint32_t CTPfragment::detEvtTypeExtraBits(const uint32_t* rod)
{
  ERS_DEBUG(4,"Get detector-event type extra bits from ROD Fragment");
  checkROD(rod);
  if(ctpFormatVersion(rod) == 0) {
    return ((rod[CTPdataformat::Helper::DetectorTypePos] >> CTPdataformat::DetEvtTypeExtraBitsShift_v0 ) 
	    & CTPdataformat::DetEvtTypeExtraBitsMask_v0);
  } else {
    return ((rod[CTPdataformat::Helper::DetectorTypePos] >> CTPdataformat::DetEvtTypeExtraBitsShift_v1 ) 
	    & CTPdataformat::DetEvtTypeExtraBitsMask_v1);     
  }
}

uint32_t CTPfragment::detEvtTypeExtraBits(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get detector-event type extra bits from ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  const uint32_t* rod;
  rob->rod_start(rod);
  return detEvtTypeExtraBits(rod);
}

uint32_t CTPfragment::hltCounter(const uint32_t* rod)
{
  ERS_DEBUG(4,"Get HLT counter from ROD Fragment");
  checkROD(rod);
  return detEvtTypeExtraBits(rod);
}

uint32_t CTPfragment::hltCounter(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get HLT counter from ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  const uint32_t* rod;
  rob->rod_start(rod);
  return hltCounter(rod);
}

uint32_t CTPfragment::lvl1AcceptBunch(const uint32_t* rod)
{
  ERS_DEBUG(4,"Get position of LVL1-Accept Bunch from ROD Fragment");
  checkROD(rod);
  if(ctpFormatVersion(rod) == 0) {
    return ((rod[CTPdataformat::Helper::DetectorTypePos]  >> CTPdataformat::L1APositionShift_v0 ) 
	    & CTPdataformat::L1APositionMask);
  } else if (ctpFormatVersion(rod) == 1){
    return ((rod[CTPdataformat::Helper::FormatVersionPos]  >> CTPdataformat::L1APositionShift_v1 ) 
	    & CTPdataformat::L1APositionMask);
  } else {
    return ((rod[CTPdataformat::Helper::FormatVersionPos]  >> CTPdataformat::L1APositionShift_v2 ) 
	    & CTPdataformat::L1APositionMask);
  }
}

uint32_t CTPfragment::lvl1AcceptBunch(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get position of LVL1-Accept Bunch from ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  const uint32_t* rod;
  rob->rod_start(rod);
  return lvl1AcceptBunch(rod);
}

uint32_t CTPfragment::ctpFormatVersion(const uint32_t* rod)
{
  ERS_DEBUG(4,"Get CTP format version from ROD Fragment");
  checkROD(rod);

  return ((rod[CTPdataformat::Helper::FormatVersionPos] >> CTPdataformat::CTPFormatVersionShift ) 
   	  & CTPdataformat::CTPFormatVersionMask);
  // return 3;
}

uint32_t CTPfragment::ctpFormatVersion(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get CTP format version from ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  const uint32_t* rod;
  rob->rod_start(rod);
  return ctpFormatVersion(rod);
}

uint32_t CTPfragment::numberExtraPayloadWords(const uint32_t* rod)
{
  ERS_DEBUG(4,"Get number of extra payload words from ROD Fragment");
  checkROD(rod);
  if(ctpFormatVersion(rod) < 2) {
    return 0;
  } else {
    return ((rod[CTPdataformat::Helper::FormatVersionPos]  >> CTPdataformat::ProgrammableExtraWordsShift_v2 ) 
	    & CTPdataformat::ProgrammableExtraWordsMask_v2);     
  }
}

uint32_t CTPfragment::numberExtraPayloadWords(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get number of extra payload words from ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  const uint32_t* rod;
  rob->rod_start(rod);
  return numberExtraPayloadWords(rod);
}

void CTPfragment::time(const uint32_t* rod,uint32_t& sec, uint32_t& nsec)
{
  ERS_DEBUG(4,"Get time from CTP ROD Fragment");
  checkROD(rod);
  sec = rod[rod[CTPdataformat::Helper::HeaderSizePos] + CTPdataformat::TimeSecondsPos];
  nsec = (rod[rod[CTPdataformat::Helper::HeaderSizePos] + CTPdataformat::TimeNanosecondsPos] >> 
	  CTPdataformat::TimeNanosecondsOffset) * CTPdataformat::TimeNanosecondsTicks;
  return;
}

void CTPfragment::time(const eformat::ROBFragment<const uint32_t*>* rob,uint32_t& sec, uint32_t& nsec)
{
  ERS_DEBUG(4,"Get time from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  const uint32_t* rod;
  rob->rod_start(rod);
  uint32_t size = rob->rod_fragment_size_word();
  // ...+1) >= size ): avoid compiler warnings '0 > uint never true'
  if( (CTPdataformat::TimeSecondsPos+1) >= (size+1) || 
      (CTPdataformat::TimeNanosecondsPos+1) >= (size+1) ) {
    throw CTPFRAGMENT_NO_TIME_STAMP();
  }
  time(rod,sec,nsec);
}

void CTPfragment::streamTags(const uint8_t& l1tt, std::vector<eformat::helper::StreamTag> & streamtags)
{
  ERS_DEBUG(4,"Construct StreamTag objects from LVL1 trigger-type "<<l1tt);
  streamtags.clear();
  if( l1tt >> CTPdataformat::Helper::TTPhysicsCalibrationBit ){
    //Physics
    for(uint32_t bit = 0; bit < CTPdataformat::Helper::TTPhysicsCalibrationBit; ++bit) {
      if( (1<<bit) & l1tt ) {
	eformat::helper::StreamTag t;
	t.type = "Physics";
	t.obeys_lumiblock = true;
	std::stringstream name;
	name << "L1TT-b"<<bit;
	t.name = name.str();
	streamtags.push_back(t);
      }
    }    
  }else{
    //Calibration
    eformat::helper::StreamTag t;
    t.type = "Calibration";
    t.obeys_lumiblock = false;
    std::stringstream name;
    name << "L1TT-det";
    uint32_t detector = ( (l1tt >> CTPdataformat::Helper::TTCalibrationDetectorCodeShift) & 
			  CTPdataformat::Helper::TTCalibrationDetectorCodeMask);
    name << detector;
    t.name = name.str();
    streamtags.push_back(t);
  } 	
}

std::vector<uint32_t> CTPfragment::triggerDecision(const eformat::ROBFragment<const uint32_t*>* rob)
{
  return triggerDecisionAfterVeto(rob);
}

std::vector<uint32_t> CTPfragment::triggerDecisionAfterVeto(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get trigger decision from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  return getTriggerWords(TAV,rob);
}

std::vector<uint32_t> CTPfragment::triggerDecisionAfterPrescales(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get trigger decision after prescales, before veto from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  return getTriggerWords(TAP,rob);
}

std::vector<uint32_t> CTPfragment::triggerDecisionBeforePrescales(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get trigger decision before prescales from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  return getTriggerWords(TBP,rob);
}

std::vector<uint32_t> CTPfragment::patternInTimeTriggerInputs(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get PIT trigger inputs from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  return getTriggerWords(PIT,rob);
}

uint32_t CTPfragment::patternInTimeAuxWord(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get auxiliary PIT word from CTP ROB Fragment");
  
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  std::vector<uint32_t> vec = getTriggerWords(PITAUX,rob);
  return vec.at(0); 
}

std::vector<uint32_t> CTPfragment::triggerDecision(const eformat::ROBFragment<const uint32_t*>* rob, uint32_t bunchCrossing)
{
  return triggerDecisionAfterVeto(rob, bunchCrossing);
}

std::vector<uint32_t> CTPfragment::triggerDecisionAfterVeto(const eformat::ROBFragment<const uint32_t*>* rob, uint32_t bunchCrossing)
{
  ERS_DEBUG(4,"Get trigger decision from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  return getTriggerWords(TAV,rob, bunchCrossing);
}

std::vector<uint32_t> CTPfragment::triggerDecisionAfterPrescales(const eformat::ROBFragment<const uint32_t*>* rob, uint32_t bunchCrossing)
{
  ERS_DEBUG(4,"Get trigger decision after prescales, before veto from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  return getTriggerWords(TAP,rob, bunchCrossing);
}

std::vector<uint32_t> CTPfragment::triggerDecisionBeforePrescales(const eformat::ROBFragment<const uint32_t*>* rob, uint32_t bunchCrossing)
{
  ERS_DEBUG(4,"Get trigger decision before prescales from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  return getTriggerWords(TBP,rob, bunchCrossing);
}

std::vector<uint32_t> CTPfragment::patternInTimeTriggerInputs(const eformat::ROBFragment<const uint32_t*>* rob, uint32_t bunchCrossing)
{
  ERS_DEBUG(4,"Get PIT trigger inputs from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  return getTriggerWords(PIT,rob, bunchCrossing);
}

uint16_t CTPfragment::bcid(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get BCID from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  std::vector<uint32_t> trigWord = getTriggerWords(PITAUX,rob);
  if(trigWord.size() == 0) {
    std::string what = "last PIT word not found.";
    throw CTPFRAGMENT_GENERIC_ISSUE(what.c_str());
  }

  const uint32_t* rod;
  rob->rod_start(rod);
  checkROD(rod);

  uint16_t bcid(0);
  uint32_t formatV = ctpFormatVersion(rod);
  if( formatV == 0) {
    bcid = static_cast<uint16_t>((trigWord[0] >> CTPdataformat::BcidShift_v0) & CTPdataformat::BcidMask_v0);
  } else if ((formatV > 0) && (formatV <4)) {
    bcid = static_cast<uint16_t>((trigWord[0] >> CTPdataformat::BcidShift_v0) & CTPdataformat::BcidMask_v1);
  } else {
    bcid = static_cast<uint16_t>((trigWord[0] >> CTPdataformat::BcidShift_v0) & CTPdataformat::BcidMask_v4);
  }

  return bcid;
}

uint16_t CTPfragment::bunchGroup(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get Bunch Group Info from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  std::vector<uint32_t> trigWord = getTriggerWords(PITAUX,rob);
  if(trigWord.size() == 0) {
    std::string what = "last PIT word not found.";
    throw CTPFRAGMENT_GENERIC_ISSUE(what.c_str());
  }

  const uint32_t* rod;
  rob->rod_start(rod);
  checkROD(rod);

  uint16_t bg(0);
  uint32_t formatV = ctpFormatVersion(rod);
  if( formatV == 0) {
    bg = static_cast<uint16_t>((trigWord[0] >> CTPdataformat::BunchGroupShift_v0) & CTPdataformat::BunchGroupMask_v0);
  } else if ((formatV > 0) && (formatV <4)) {
    bg = static_cast<uint16_t>((trigWord[0] >> CTPdataformat::BunchGroupShift_v1) & CTPdataformat::BunchGroupMask_v1);
  } else {
    bg = static_cast<uint16_t>((trigWord[0] >> CTPdataformat::BunchGroupShift_v4) & CTPdataformat::BunchGroupMask_v4);
  }
  return bg;
}

std::vector<bool> CTPfragment::randomTriggers(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get random triggers from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROBFragment");
  }
  checkROB(rob);
  std::vector<uint32_t> trigWord = getTriggerWords(PITAUX,rob);
  if(trigWord.size() == 0) {
    std::string what = "last PIT word not found.";
    throw CTPFRAGMENT_GENERIC_ISSUE(what.c_str());
  }

  const uint32_t* rod;
  rob->rod_start(rod);
  checkROD(rod);

  std::vector<bool> rnd(0);

  if(ctpFormatVersion(rod) < 4) {
    uint32_t trigMask = (trigWord[0] >> CTPdataformat::RandomTrigShift_v0) & CTPdataformat::RandomTrigMask_v0;
    rnd.resize(2, false);
    rnd[0] = trigMask & 1; // random trigger #1
    rnd[1] = trigMask & 2; // random trigger #2
  } else {
    uint32_t trigMask = (trigWord[0] >> CTPdataformat::RandomTrigShift_v0) & CTPdataformat::RandomTrigMask_v4;
    rnd.resize(4, false);
    rnd[0] = trigMask & 1; // random trigger #1
    rnd[1] = trigMask & 2; // random trigger #2
    rnd[2] = trigMask & 4; // random trigger #3
    rnd[3] = trigMask & 8; // random trigger #4
  }
  return rnd;
}

std::vector<bool> CTPfragment::prescaledClockTriggers(const eformat::ROBFragment<const uint32_t*>* rob)
{
  ERS_DEBUG(4,"Get random triggers from CTP ROB Fragment");
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROB Fragment");
  }
  checkROB(rob);
  std::vector<uint32_t> trigWord = getTriggerWords(PITAUX,rob);
  if(trigWord.size() == 0) {
    std::string what = "last PIT word not found.";
    throw CTPFRAGMENT_GENERIC_ISSUE(what.c_str());
  }

  const uint32_t* rod;
  rob->rod_start(rod);
  checkROD(rod);

  std::vector<bool> pscl(0);
  
  if(ctpFormatVersion(rod) < 4) {
    uint32_t trigMask = (trigWord[0] >> CTPdataformat::PrescaledClockShift_v0) & CTPdataformat::PrescaledClockMask_v0;
    pscl.resize(2, false);
    pscl[0] = trigMask & 1; // random trigger #1
    pscl[1] = trigMask & 2; // random trigger #2
  } else {
    // do nothing and give back empty vector
  }

  return pscl;
}

void checkROB(const eformat::ROBFragment<const uint32_t*>* rob)
{
  if(!rob) {
    throw CTPFRAGMENT_NULL_POINTER("ROB Fragment");
  }
  try {
    rob->check();
  }  catch (eformat::Issue& ex) {
    std::string what = "ROB fragment invalid ( caught \'";
    what += ex.what();
    what += "\' ) : ";
    throw CTPFRAGMENT_GENERIC_ISSUE(what.c_str());
  }
}

void checkROD(const uint32_t* rod)
{
  if(!rod) {
    throw CTPFRAGMENT_NULL_POINTER("ROD Fragment");
  }
  if(rod[CTPdataformat::Helper::HeaderMarkerPos] != eformat::ROD) { 
    throw CTPFRAGMENT_INCONSISTENCY("ROD Header Marker",rod[CTPdataformat::Helper::HeaderMarkerPos],eformat::ROD);
  }
  if( (rod[CTPdataformat::Helper::FormatVersionPos] >> 16) != (eformat::DEFAULT_ROD_VERSION >> 16)) {
    throw CTPFRAGMENT_INCONSISTENCY("ROD Version Number",
				    rod[CTPdataformat::Helper::FormatVersionPos]>>16,
				    eformat::DEFAULT_ROD_VERSION>>16);
  }
  if( (rod[CTPdataformat::Helper::SourceIdPos] >> 16) != eformat::TDAQ_CTP ) {
    throw CTPFRAGMENT_INCONSISTENCY("Source ID",rod[CTPdataformat::Helper::SourceIdPos]>>16, eformat::TDAQ_CTP);
  }
}

std::vector<uint32_t> getTriggerWords(WordType type,
				      const eformat::ROBFragment<const uint32_t*>* rob)
{
  const uint32_t* rod;
  rob->rod_start(rod);
  checkROD(rod);
  uint32_t formatV = CTPfragment::ctpFormatVersion(rod);
      
  uint32_t nWords = 0;
  uint32_t offset = 0;

  uint32_t l1aBunch = 0;
  const uint32_t daq_id = (eformat::TDAQ_CTP << 16) | 0x0;
  const uint32_t lvl2_id = (eformat::TDAQ_CTP << 16) | 0x1;

  bool isLevel2(false);

  switch (rod[CTPdataformat::Helper::SourceIdPos])
    {
    case daq_id : // DAQ fragment - can contain multiple bunches, we need to find the L1A bunch
      l1aBunch = CTPfragment::lvl1AcceptBunch(rod);
      break;
    case lvl2_id : // LVL2 fragment - can only contain one single bunch, the L1A bunch
      l1aBunch = 0;
      isLevel2=true;
      break;
    default:
      throw CTPFRAGMENT_INCONSISTENCY("Source ID",rod[CTPdataformat::Helper::SourceIdPos],daq_id);
      break;
    }

  bool padding(false); // pad with zeros for >=v5, LVL2 fragment, PIT, TAP

  switch (type) {
  case PIT:
    if(formatV < 4) {
      nWords = CTPdataformat::PITwords_v0;
      offset = CTPdataformat::PITpos;
    } else {
      nWords = CTPdataformat::PITwords_v4;
      offset = CTPdataformat::PITpos;
    }
    if ((formatV > 4) && isLevel2) {
      padding=true;
    }
    break;
  case PITAUX:
    nWords = 1;
    if(formatV < 4) {
      offset = CTPdataformat::PITpos + (CTPdataformat::AuxPITwordPos_v0-1);
    } else {
      offset = CTPdataformat::PITpos + (CTPdataformat::AuxPITwordPos_v4-1);
    }
    if ((formatV > 4) && isLevel2) {
      offset = CTPdataformat::PITpos;
      padding=true;
    }
    break;
  case TBP:
    if(formatV < 4) {
      offset = CTPdataformat::TBPpos_v0;
      nWords = CTPdataformat::TBPwords_v0;
    } else {
      offset = CTPdataformat::TBPpos_v4;
      nWords = CTPdataformat::TBPwords_v4;
    }
    if ((formatV > 4) && isLevel2) {
      offset = CTPdataformat::TBPpos_v5;
    }
    break;
  case TAP:
    if(formatV < 4) {
      offset = CTPdataformat::TAPpos_v0;
      nWords = CTPdataformat::TAPwords_v0;
    } else {
      offset = CTPdataformat::TAPpos_v4;
      nWords = CTPdataformat::TAPwords_v4;
    }
    if ((formatV > 4) && isLevel2) {
      offset = CTPdataformat::TBPpos_v5;
      padding=true;
    }
    break;
  case TAV:
    if(formatV < 4) {
      offset = CTPdataformat::TAVpos_v0;
      nWords = CTPdataformat::TAVwords_v0;
    } else {
      offset = CTPdataformat::TAVpos_v4;
      nWords = CTPdataformat::TAVwords_v4;
    }
    if ((formatV > 4) && isLevel2) {
      offset = CTPdataformat::TAVpos_v5;
    }

    break;
  default:
    std::stringstream what;
    what << "Invalid trigger-word type \'";
    what << type;
    what << "\' ) : ";
    throw CTPFRAGMENT_GENERIC_ISSUE(what.str().c_str());
    break;
  }
   
  uint32_t payloadSize = rob->rod_fragment_size_word(); 
  payloadSize -= rob->rod_trailer_size_word();
  payloadSize -= rob->rod_nstatus();
  payloadSize -= CTPfragment::numberExtraPayloadWords(rod); //'Payload' is only trigger words here.
  std::vector<uint32_t> vec;
   
  uint32_t maxIndex(0);
  if(formatV < 4) {
    maxIndex= l1aBunch * CTPdataformat::DAQwordsPerBunch_v0 + offset + (nWords-1);
  } else {
    maxIndex= l1aBunch * CTPdataformat::DAQwordsPerBunch_v4 + offset + (nWords-1);
  } 
  if ((formatV > 4) && isLevel2) {
    if ( (type==TBP) || (type==TAV) ) {
      maxIndex= l1aBunch * CTPdataformat::LVL2wordsPerBunch_v5 + offset + (nWords-1);
    } else {
      maxIndex= l1aBunch * CTPdataformat::LVL2wordsPerBunch_v5 + offset - 1;
    }
  }

  if( maxIndex >= payloadSize ) {
    std::cout << "A: No more trigger words, requested to read up to " << maxIndex << " but only " << payloadSize << " words available.\n";
    return vec;
  }
  for(uint32_t i = 0; i < nWords; ++i) {
    uint32_t index(0);
    if(formatV < 4) {
      index = l1aBunch * CTPdataformat::DAQwordsPerBunch_v0 + offset + i;
    } else {
      index = l1aBunch * CTPdataformat::DAQwordsPerBunch_v4 + offset + i;
    }
    if ((formatV > 4) && isLevel2 && padding) {
      vec.push_back(0);
    } else {
      vec.push_back(  rod[rod[CTPdataformat::Helper::HeaderSizePos]+index] );
    }
  }
  return vec;
}

std::vector<uint32_t> getTriggerWords(WordType type,
				      const eformat::ROBFragment<const uint32_t*>* rob,
				      uint32_t bunchCrossing)
{
  const uint32_t* rod;
  rob->rod_start(rod);
  checkROD(rod);
  uint32_t formatV = CTPfragment::ctpFormatVersion(rod);

  uint32_t nWords = 0;
  uint32_t offset = 0;

  const uint32_t daq_id = (eformat::TDAQ_CTP << 16) | 0x0;
  const uint32_t lvl2_id = (eformat::TDAQ_CTP << 16) | 0x1;

  uint32_t l1aBunch(bunchCrossing);

  bool isLevel2(false);

  switch (rod[CTPdataformat::Helper::SourceIdPos])
    {
    case daq_id : // DAQ fragment
      // keep l1aBunch as it is and do nothing
      break;
    case lvl2_id : // LVL2 fragment - can only contain one single bunch, the L1A bunch
      l1aBunch = 0;
      isLevel2=true;
      break;
    default:
      throw CTPFRAGMENT_INCONSISTENCY("Source ID",rod[CTPdataformat::Helper::SourceIdPos],daq_id);
      break;
    }

  bool padding(false); // pad with zeros for >=v5, LVL2 fragment, PIT, TAP

  switch (type) {
  case PIT:
    if(formatV < 4) {
      nWords = CTPdataformat::PITwords_v0;
    } else {
      nWords = CTPdataformat::PITwords_v4;
    }
    offset = CTPdataformat::PITpos;
    if ((formatV > 4) && isLevel2) {
      padding=true;
    }
    break;
  case PITAUX:
    nWords = 1;
    if(formatV < 4) {
      offset = CTPdataformat::PITpos + (CTPdataformat::AuxPITwordPos_v0-1);
    } else {
      offset = CTPdataformat::PITpos + (CTPdataformat::AuxPITwordPos_v4-1);
    }
    if ((formatV > 4) && isLevel2) {
      offset= CTPdataformat::PITpos;
      padding=true;
    }
    break;
  case TBP:
    if(formatV < 4) {
      offset = CTPdataformat::TBPpos_v0;
      nWords = CTPdataformat::TBPwords_v0;
    } else {
      offset = CTPdataformat::TBPpos_v4;
      nWords = CTPdataformat::TBPwords_v4;
    }
    if ((formatV > 4) && isLevel2) {
      offset = CTPdataformat::TBPpos_v5;
    }
    break;
  case TAP:
    if(formatV < 4) {
      offset = CTPdataformat::TAPpos_v0;
      nWords = CTPdataformat::TAPwords_v0;
    } else {
      offset = CTPdataformat::TAPpos_v4;
      nWords = CTPdataformat::TAPwords_v4;
    }
    if ((formatV > 4) && isLevel2) {
      offset = CTPdataformat::TBPpos_v5;
      padding=true;
    }
    break;
  case TAV:
    if(formatV < 4) {
      offset = CTPdataformat::TAVpos_v0;
      nWords = CTPdataformat::TAVwords_v0;
    } else {
      offset = CTPdataformat::TAVpos_v4;
      nWords = CTPdataformat::TAVwords_v4;
    }
    if ((formatV > 4) && isLevel2) {
      offset = CTPdataformat::TAVpos_v5;
    }

    break;
  default:
    std::stringstream what;
    what << "Invalid trigger-word type \'";
    what << type;
    what << "\' ) : ";
    throw CTPFRAGMENT_GENERIC_ISSUE(what.str().c_str());
    break;
  }

  uint32_t payloadSize = rob->rod_fragment_size_word();
  payloadSize -= rob->rod_trailer_size_word();
  payloadSize -= rob->rod_nstatus();
  payloadSize -= CTPfragment::numberExtraPayloadWords(rod); //'Payload' is only trigger words here.
  std::vector<uint32_t> vec;
  
  uint32_t maxIndex(0);
  if(formatV < 4) {
    maxIndex = l1aBunch * CTPdataformat::DAQwordsPerBunch_v0 + offset + (nWords-1);
  } else {
    maxIndex = l1aBunch * CTPdataformat::DAQwordsPerBunch_v4 + offset + (nWords-1);
  }
  if ((formatV > 4) && isLevel2) {
    if ( (type==TBP) || (type==TAV) ) {
      maxIndex= l1aBunch * CTPdataformat::LVL2wordsPerBunch_v5 + offset + (nWords-1);
    } else {
      maxIndex= l1aBunch * CTPdataformat::LVL2wordsPerBunch_v5 + offset - 1;
    }
  }

  if( maxIndex >= payloadSize ) {
    std::cout << "B: No more trigger words, requested to read up to " << maxIndex << " but only " << payloadSize << " words available.\n";
    return vec;
  }
  for(uint32_t i = 0; i < nWords; ++i) {
    uint32_t index(0);
    if(formatV < 4) {
      index = l1aBunch * CTPdataformat::DAQwordsPerBunch_v0 + offset + i;
    } else {
      index = l1aBunch * CTPdataformat::DAQwordsPerBunch_v4 + offset + i;
    }
    if ((formatV > 4) && isLevel2 && padding) {
      vec.push_back(0);
    } else {
      vec.push_back(  rod[rod[CTPdataformat::Helper::HeaderSizePos]+index] );
    }
  }
  return vec;
}

std::vector<uint32_t> CTPfragment::getAllWords(const eformat::ROBFragment<const uint32_t*>* rob)
{
  const uint32_t* rod;
  rob->rod_start(rod);

  uint32_t payloadSize = rob->rod_fragment_size_word(); 
  uint32_t maxIndex = payloadSize;
  std::vector<uint32_t> vec;
  
  for(uint32_t i = 0; i < maxIndex; ++i) {
    vec.push_back(  rod[i] );
  }
  return vec;
}
