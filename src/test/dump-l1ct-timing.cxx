/**
 * Modified version of StorageReader test program 
 *
 */

#include <iostream>
#include <iomanip>
#include <ios>
#include <memory>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <time.h>
#include <stdint.h>

int trigBits[60][256];
unsigned int nBC;
uint32_t l1a;
bool firstbconly;

#include "EventStorage/pickDataReader.h"
#include "eformat/eformat.h"
//#include "cmdl/cmdargs.h"
#include <boost/program_options.hpp>

#include "CTPfragment/CTPfragment.h"
#include "CTPfragment/CTPdataformat.h"
#include "CTPfragment/MuCTPIdataformat.h"

const size_t MAX_ROB_COUNT = 3200; // maximum number of ROB=ROD fragments

void dumpCtp(const eformat::ROBFragment<const uint32_t*>* rbf)
{	
  l1a = CTPfragment::lvl1AcceptBunch(rbf);

  int evTrigBits[60][256];
  for (int i=0; i<60; i++) {
    for(int j=0; j<256; j++) {evTrigBits[i][j]=0;}
  }

  //now dump all the bits in a matrix
  nBC=0;
  std::vector<uint32_t> bits;
  for(int bc=0; bc<64; bc++) {
    bits = CTPfragment::triggerDecisionBeforePrescales(rbf, bc);
    if(bits.size()>0) {
      nBC++;
      int nBITs=255;
      for(int i=bits.size()-1; i>=0; i--) {
	uint32_t word=bits.at(i);
	for (int bit = 31; bit>=0; --bit) {
	  if( (word >> bit) & 1 ) {evTrigBits[bc][nBITs]++;}
	  nBITs--;
	}
      }
    }
    bits.clear();
  }

  //evTrigBits has all the fired BCs for that event.  Now just take the first and increment trigBits accordingly.
  int isNotFirst[256];
  for(int i=0; i<256; i++) {isNotFirst[i]=0;}
  for (int i=0; i<60; i++) {
    for(int j=0; j<256; j++) {
      if(evTrigBits[i][j]>0 && isNotFirst[j]==0) {
	trigBits[i][j]++;
	if(firstbconly==true) isNotFirst[j]=1;
      }
    }
  }
}

int main (int argc, char *argv[])
{
  using namespace eformat;
  std::string fName="";

  std::string outputFile="";
  std::vector<std::string> inputFiles;
  int nEvents=10000000;
  bool allBC=false;

  // parse command line parameters
  boost::program_options::options_description 
    desc("Text dumper for CTP/MuCTPI raw data fragments");
  desc.add_options()
    ("help,h", "Help Message")
    ("allBCs,a", "Count all BC's for timing (default is first only)")
    ("nEvents,n", boost::program_options::value<int>(), "Number of events to skim (Default -1=ALL)")
    ("outputFile,o",  boost::program_options::value< std::string >(), "Name of output text file")
    ("inputFiles,i", boost::program_options::value< std::vector<std::string> >(), "List of input files")
    ;

  boost::program_options::positional_options_description p;
  p.add("inputFiles", -1);
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  boost::program_options::notify(vm);
  
  if(vm.count("help")) {
    std::cout << desc << std::endl;
    return EXIT_SUCCESS;
  }
  if(vm.count("allBCs")) {
    allBC=true;
  }
  if(vm.count("nEvents")) {
    nEvents = vm["nEvents"].as<int>();
  }
  if(vm.count("outputFile")) {
    outputFile = vm["outputFile"].as< std::string >();
  }
  if(vm.count("inputFiles")) {
    inputFiles = vm["inputFiles"].as< std::vector<std::string> >();
  }
  else {
    std::cout << "No filename specified!" << std::endl;
    std::cout << desc << std::endl;
    return EXIT_FAILURE;
  }
  
  int eventCounter=0;

  bool makeFile=false;
  if(outputFile != "") makeFile=true;

  firstbconly=!allBC;

  for (int i=0; i<60; i++) {
    for(int j=0; j<256; j++) {trigBits[i][j]=0;}
  }

  DataReader *pDR = 0;
  for(unsigned int f=0; f<inputFiles.size() && eventCounter<nEvents; f++) {
    fName = static_cast<std::string>(inputFiles.at(f));

    pDR = pickDataReader(fName);
    if(!pDR) {
      std::cout << "Problem opening or reading this file!\n";
      return -1;
    }
    if(!pDR->good()) {
      std::cout << "There is a problem!" << std::endl;
      return -1;
    }
    // This info (for instance) is available without penalty at start time.

    std::cout << "Processing File " << fName << std::endl;

    while(pDR->good() && eventCounter<nEvents) 
      {
	unsigned int eventSize;    
	char *buf;
	
	DRError ecode = pDR->getData(eventSize,&buf);
	if(DROK != ecode) {
	  std::cout << "Can't read from file!" << std::endl;
	  break;
	}
	++eventCounter;

	// now start the eformat business

	uint32_t* fragment = reinterpret_cast<uint32_t*>(buf);

	try {
	  switch ((eformat::HeaderMarker)(fragment[0]))
	    {
	    case FULL_EVENT: {
	      FullEventFragment<const uint32_t*> fe(fragment);
	      fe.check();
	      break; }
	    default:
	      std::cerr << "Fragment type not recognized after conversion, skip.";
	      delete [] buf;
	      continue;
	    }

	  // now that we have a valid fragment, let us unpack down to ROBs
	  std::vector<std::shared_ptr<const uint32_t>> robs;
	  get_robs (fragment,robs);
	  size_t Nrob = robs.size();
	  if(eventCounter%50==0) std::cout << "Scanning event " << eventCounter << " with " << Nrob << " fragments" << std::endl;

	  //Check progress!
	  if(eventCounter%1000==1 && eventCounter>1) {
	    std::cout << eventCounter-1 << " Events Processed" << std::endl;
	    std::cout << std::endl << "                                    Trigger Bits Per BC" << std::endl;

	    int max=0;
	    int nonzero[256];
	    for(int i=0; i<256; i++) {
	      nonzero[i]=0;
	      for(unsigned int j=0; j<nBC; j++) {
		if(trigBits[j][i]>max) max=trigBits[j][i];
		if(trigBits[j][i]!=0) nonzero[i]=1;
	      }
	    }  
	    int w=2;
	    if(max>999) w=5;
	    else if(max>99) w=4;
	    else if(max>9) w=3;

	    std::cout << "     ";
	    std::cout << std::setw(w);
	    std::cout << std::endl << "    B ";
	    for(unsigned int y=0; y<nBC; y++) {int tens = y/10; std::cout << std::setw(w) << tens;} 
	    std::cout << std::endl << "TBP C ";
	    for(unsigned int y=0; y<nBC; y++) {int ones = y%10; std::cout << std::setw(w) << ones;} 
	    std::cout << std::endl << std::endl;
  

	    for(int i=0; i<256; i++) {
	      if(nonzero[i]==1) {
		std::cout << std::setw(3) << std::right << std::setfill(' ') << i << "   ";
		for(unsigned int j=0; j<nBC; j++) {
		  if(trigBits[j][i]!=0) std::cout << std::setw(w) << trigBits[j][i];
		  else {
		    for (int t=1; t<w; t++) std::cout << " ";
		    if(j==l1a) std::cout << std::setw(1) << "+";
		    else std::cout << std::setw(1) << "-";
		  }
		}
		std::cout << std::endl;
	      }
	    }
	    std::cout << std::endl;
	  }
	  //End check progress

          bool hasCTPFragment=false;
          const uint32_t daq_id = (eformat::TDAQ_CTP << 16) | 0x0;

	  for (size_t irob=0; irob<Nrob && hasCTPFragment==false; irob++) {
	      ROBFragment<const uint32_t*> *rbf = new ROBFragment<const uint32_t*>(robs[irob].get());
	    if( (rbf->rod_source_id()>>16) == eformat::TDAQ_CTP && rbf->rod_source_id() == daq_id) {
	      // CTP DAQ fragment
	      hasCTPFragment=true;
	      dumpCtp(rbf);
	    } 
	    delete rbf;
	  }
	} catch (eformat::Issue& ex) {
	  std::cerr << "Uncaught eformat issue: " << ex.what() << std::endl;
	  delete [] buf;
	  continue;
	} catch (ers::Issue& ex) {
	  std::cerr << "Uncaught ERS issue: " << ex.what() << std::endl;
	  delete [] buf;
	  continue;
	} catch (std::exception& ex) {
	  std::cerr << "Uncaught std exception: " << ex.what() << std::endl;
	  delete [] buf;
	  continue;
	} catch (...) {
	  std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
	  delete [] buf;
	  continue;
	}
	// end of processing for this data block
	delete [] buf;
      }
  }
  std::cout << eventCounter << " Events Processed" << std::endl;
  std::cout << std::endl << "                                    Trigger Bits Per BC" << std::endl;

  int max=0;
  int nonzero[256];
  for(int i=0; i<256; i++) {
    nonzero[i]=0;
    for(unsigned int j=0; j<nBC; j++) {
      if(trigBits[j][i]>max) max=trigBits[j][i];
      if(trigBits[j][i]!=0) nonzero[i]=1;
    }
  }  
  int w=2;
  if(max>999) w=5;
  else if(max>99) w=4;
  else if(max>9) w=3;

  std::cout << "     ";
  std::cout << std::setw(w);
  std::cout << std::endl << "    B ";
  for(unsigned int y=0; y<nBC; y++) {int tens = y/10; std::cout << std::setw(w) << tens;} 
  std::cout << std::endl << "TBP C ";
  for(unsigned int y=0; y<nBC; y++) {int ones = y%10; std::cout << std::setw(w) << ones;} 
  std::cout << std::endl << std::endl;
  

  for(int i=0; i<256; i++) {
    if(nonzero[i]==1) {
      std::cout << std::setw(3) << std::right << std::setfill(' ') << i << "   ";
      for(unsigned int j=0; j<nBC; j++) {
	if(trigBits[j][i]!=0) std::cout << std::setw(w) << trigBits[j][i];
	else {
	  for (int t=1; t<w; t++) std::cout << " ";
	  if(j==l1a) std::cout << std::setw(1) << "+";
	  else std::cout << std::setw(1) << "-";
	}
      }
      std::cout << std::endl;
    }
  }
  std::cout << std::endl;

  if(makeFile) {
    std::ofstream outfile( outputFile.data() );
    std::cout << "File " << outputFile << " opened." << std::endl;
    outfile << nBC << "\t" << l1a << std::endl;
    for(int i=0; i<256; i++) {
      if(nonzero[i]==1) {
	outfile << std::setw(3) << std::right << std::setfill(' ') << i << "   ";
	for(unsigned int j=0; j<nBC; j++) {
	  outfile << std::setw(w) << trigBits[j][i];
	}
	outfile << std::endl;
      }
    }
    outfile.close();
  }
  delete pDR;
}
