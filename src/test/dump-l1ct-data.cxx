/**
 * Modified version of StorageReader test program 
 *
 */

#include <iostream>
#include <iomanip>
#include <ios>
#include <memory>
#include <string>
#include <fstream>
#include <sstream>
#include <time.h>
#include <sys/types.h>  // for u_int

#include "EventStorage/pickDataReader.h"
#include "eformat/eformat.h"
#include "eformat/write/eformat.h"

//#include "cmdl/cmdargs.h"
#include <boost/program_options.hpp>

#include "CTPfragment/CTPfragment.h"
#include "CTPfragment/CTPdataformat.h"
#include "CTPfragment/MuCTPIdataformat.h"

const size_t MAX_ROB_COUNT = 3200; // maximum number of ROB=ROD fragments

int lowEvent=1;
int nEvents=-1;
std::string filename="";
bool ctpOnly=false;
bool daqOnly=false;
bool allBunches=false;

static const int maxwindow = 64;

void dumpMuctpi(const eformat::ROBFragment<const uint32_t*>* rbf)
{	
  const uint32_t daq_id = (eformat::TDAQ_MUON_CTP_INTERFACE << 16) | 0x0;
  const uint32_t lvl2_id = (eformat::TDAQ_MUON_CTP_INTERFACE << 16) | 0x1;
	
  std::cout << "---------------------------------------------------------------------------------" << std::endl;
  std::cout << std::setw(20) << std::right << std::setfill(' ');
  std::cout << std::hex << std::showbase;

  if(rbf->rod_source_id() == daq_id) std::cout << "MuCTPI DAQ fragment found!" << std::endl;
  else if(rbf->rod_source_id() == lvl2_id) std::cout << "MuCTPI LVL2 fragment found!" << std::endl;
  else std::cout << "Unknown fragment found!" << std::endl;

  uint32_t head_bcid = rbf->rod_bc_id();
  uint32_t head_l1id = rbf->rod_lvl1_id();
  uint32_t head_tt = rbf->rod_lvl1_trigger_type();
  uint32_t head_runno = rbf->rod_run_no();

  std::vector<uint32_t> allwords = CTPfragment::getAllWords(rbf);
  std::cout << "ROD Fragment    (All) | " << std::hex;
  for(unsigned int y=0; y<allwords.size(); y++) {
    if(y%5==0 && y!=0) std::cout << std::endl << "                      | ";
    std::cout << std::noshowbase << std::right << std::setw(8) << std::setfill('0') << allwords.at(y) << " ";
  }
  std::cout << std::showbase << std::endl;

  std::cout << "ROD Header     run no | " << head_runno << " (" << std::dec << head_runno << ")" << std::hex << std::endl;
  std::cout << "ROD Header       BCID | " << head_bcid << " (" << std::dec << head_bcid << ")" << std::hex << std::endl;
  std::cout << "ROD Header    LVL1 ID | " << head_l1id << std::endl;
  std::cout << "ROD Header  trig-type | " << head_tt << std::endl;

  int32_t payloadSize = rbf->rod_fragment_size_word(); 
  payloadSize -= rbf->rod_header_size_word();
  payloadSize -= rbf->rod_trailer_size_word();
  payloadSize -= rbf->rod_nstatus();
  const uint32_t* rod;
  rbf->rod_start(rod);
  // now loop over data and dump
  for(int32_t i = 0 ; i<payloadSize; ++i) {
    uint32_t data = rod[rod[CTPdataformat::Helper::HeaderSizePos] + i];
    // check if multi or cand word
    if((data >> MuCTPIdataformat::MultiplicityWordFlagShift) & 1) {
      std::cout << "ROD Data Mult    word | " << data << std::endl;
      uint32_t mult[MuCTPIdataformat::MultiplicityThresholdNumber];
      std::cout << std::dec;
      for(uint32_t j=0; j<MuCTPIdataformat::MultiplicityThresholdNumber; j++) {
	mult[j] = (data >> (MuCTPIdataformat::MultiplicityBits*j)) & MuCTPIdataformat::MultiplicityVal;
	std::cout << "             Pt" << j+1 << " Mult | " << mult[j] << std::endl;
      }
      uint32_t bcid = (data >> (MuCTPIdataformat::MultiplicityBits*(MuCTPIdataformat::MultiplicityBcidPos-1))) & MuCTPIdataformat::MultiplicityVal;
      std::cout << std::dec << "                 BCID | " << bcid << std::hex << std::endl;
    }
    else {
      std::cout << "ROD Data Cand    word | " << data << std::endl;

      uint32_t address = 0;
      bool candOverflow = 0;
      bool roiOverflow = 0;
      uint32_t roi = 0;
      uint32_t overlap = 0;
      uint32_t candPt = 0;
      uint32_t bcid = 0;
      bool isHighestPt = false;
      bool sentToRoIB = false;
      bool isEndcap = false;
      bool isForward = false;
      bool isBarrel = false;
      int hemi = 0;

      if(rbf->rod_source_id() != lvl2_id) {//Decode as real MuCTPI candidate word	
	address = (data >> MuCTPIdataformat::CandidateSectorAddressShift) & MuCTPIdataformat::CandidateSectorAddressMask;
	isEndcap = address & MuCTPIdataformat::EndcapAddressMask;
	isForward = (address & MuCTPIdataformat::ForwardAddressMask) && !isEndcap;
	isBarrel = !isForward && !isEndcap;
	hemi=address & MuCTPIdataformat::SectorHemisphereMask;
	candOverflow = (data >> MuCTPIdataformat::CandidateOverflowShift) & MuCTPIdataformat::CandidateOverflowMask;
	roiOverflow = (data >> MuCTPIdataformat::RoIOverflowShift) & MuCTPIdataformat::RoIOverflowMask;
	candPt = (data >> MuCTPIdataformat::CandidatePtShift) & MuCTPIdataformat::CandidatePtMask;
	bcid = (data >> MuCTPIdataformat::CandidateBcidShift) & MuCTPIdataformat::CandidateBcidMask;
	isHighestPt = (data >> MuCTPIdataformat::CandidateHighestPtShift) & MuCTPIdataformat::CandidateHighestPtMask;
	sentToRoIB = (data >> MuCTPIdataformat::CandidateSentRoIShift) & MuCTPIdataformat::CandidateSentRoIMask;
	if(isBarrel) {
	  roi = (data >> MuCTPIdataformat::RoIShift) & MuCTPIdataformat::BarrelRoIMask;
	  overlap = (data >> MuCTPIdataformat::BarrelOverlapShift) & MuCTPIdataformat::BarrelOverlapMask;
	}
	else if(isEndcap) {
	  roi = (data >> MuCTPIdataformat::RoIShift) & MuCTPIdataformat::EndcapRoIMask;
	  overlap = (data >> MuCTPIdataformat::EndcapOverlapShift) & MuCTPIdataformat::EndcapOverlapMask;
	}
	else {
	  roi = (data >> MuCTPIdataformat::RoIShift) & MuCTPIdataformat::ForwardRoIMask;
	  overlap = 0;
	}
      }
      else {//Decode as Muon RoI candidate word
	address = (data >> MuCTPIdataformat::RoIWordSectorAddressShift) & MuCTPIdataformat::RoIWordSectorAddressMask;
	isEndcap = address & MuCTPIdataformat::EndcapAddressMask;
	isForward = (address & MuCTPIdataformat::ForwardAddressMask) && !isEndcap;
	isBarrel = !isForward && !isEndcap;
	hemi=address & MuCTPIdataformat::SectorHemisphereMask;
	candPt = (data >> MuCTPIdataformat::RoIWordPtShift) & MuCTPIdataformat::RoIWordPtMask;
	if(isBarrel) roi = (data >> MuCTPIdataformat::RoIShift) & MuCTPIdataformat::BarrelRoIMask;
	else if(isEndcap) roi = (data >> MuCTPIdataformat::RoIShift) & MuCTPIdataformat::EndcapRoIMask;
	else roi = (data >> MuCTPIdataformat::RoIShift) & MuCTPIdataformat::ForwardRoIMask;
      }
      if(isBarrel) std::cout << "            Cand type | Barrel" << std::endl;
      if(isEndcap) std::cout << "            Cand type | Endcap" << std::endl;
      if(isForward) std::cout << "            Cand type | Forward" << std::endl;
      int sectorid=-1;
      if(isBarrel) {sectorid=(address >> 1) & MuCTPIdataformat::BarrelSectorIdMask;}
      else if(isEndcap) {sectorid=(address >> 1) & MuCTPIdataformat::EndcapSectorIdMask;}
      else if(isForward) {sectorid=(address >> 1) & MuCTPIdataformat::ForwardSectorIdMask;}
      std::cout << "       Cand Sector ID | " << sectorid << " (" << std::dec << sectorid << ")" << std::endl;
      std::cout << "      Cand Hemisphere | " << hemi;
      if(hemi==1) std::cout << " (pos)" << std::endl;
      else std::cout << " (neg)" << std::endl;
      std::cout << std::dec << "                  RoI | " << roi << std::endl;

      if(rbf->rod_source_id() != lvl2_id) {
	std::cout << std::boolalpha;
	std::cout << "   >2 Cand per Sector | " << candOverflow << std::endl;
	std::cout << "      >1 Cand per RoI | " << roiOverflow << std::endl;
	if (!isForward) std::cout << std::hex << "              overlap | " << overlap << std::endl;
	std::cout << std::dec << "              cand Pt | " << candPt << std::endl;
	std::cout << "                 BCID | " << bcid << std::endl;
	std::cout << "           Highest Pt | " << isHighestPt << std::endl;
	std::cout << "             Sent RoI | " << sentToRoIB << std::endl;
      }
      std::cout << std::noboolalpha << std::hex;
    }
  }
}

/**
 * Dump CTP data 
 */
void dumpCtp(const eformat::ROBFragment<const uint32_t*>* rbf)
{
  // use character array in combination with sprintf, because it is such a pain to
  // use the manipulators of cout correctly with hex output, e.g. 0x00000000.

  const uint32_t daq_id = (eformat::TDAQ_CTP << 16) | 0x0;
  const uint32_t lvl2_id = (eformat::TDAQ_CTP << 16) | 0x1;
      
  std::cout << "---------------------------------------------------------------------------------" << std::endl;

  uint32_t ctpFormatVersion = CTPfragment::ctpFormatVersion(rbf);
  bool is_l2(false);
  if(rbf->rod_source_id() == daq_id) {
    std::cout << "CTP DAQ fragment found with version " << ctpFormatVersion << std::endl;
    is_l2 = false;
  } else if(rbf->rod_source_id() == lvl2_id) {
    std::cout << "CTP LVL2 fragment with version " << ctpFormatVersion << std::endl;
    is_l2 = true;
  } else {
    std::cout << "Non-CTP fragment found!" << "\n\n";
    return;
  }

  int npits(0);
  if (ctpFormatVersion < 4) {
    npits = (CTPdataformat::PITwords_v0-1)*32;
  } else {
    npits = (CTPdataformat::PITwords_v4-1)*32;
  }

  int nitems(0);
  if (ctpFormatVersion < 4) {
    nitems = (CTPdataformat::TBPwords_v0)*32;
  } else {
    nitems = (CTPdataformat::TBPwords_v4)*32;
  }
   
  std::vector<uint32_t> allwords = CTPfragment::getAllWords(rbf);
  std::cout << "ROD Fragment    (All) | 0x";
  uint32_t headermarker(0);
  uint32_t headersize(0);
  uint32_t formatversionnumber(0);
  uint32_t sourceidentifier(0);
  for(unsigned int y=0; y<allwords.size(); y++) {
    if (y==0) headermarker=allwords.at(y);
    if (y==1) headersize=allwords.at(y);
    if (y==2) formatversionnumber=allwords.at(y);
    if (y==3) sourceidentifier=allwords.at(y);

    if(y%6==0 && y!=0) {
      printf("\n                %03d   | 0x", y);
      // std::cout << std::endl << "                " << std::right << std::setw(3) << std::setfill('0') << std::dec << y << std::hex << "   | 0x";
    }
    printf("%08x ",allwords.at(y));
    // std::cout << std::noshowbase << std::right << std::setw(8) << std::setfill('0') << allwords.at(y) << " ";
  }
  std::cout << "\n\n";


  // calculate sizes
  int32_t fragment_size = rbf->rod_fragment_size_word();
  int32_t header_size = rbf->rod_header_size_word();
  int32_t trailer_size = rbf->rod_trailer_size_word();
  int32_t status_size = rbf->rod_nstatus();
  int32_t payload_size = fragment_size-header_size-trailer_size-status_size;
  int32_t timestampwords_size=2;
  int32_t extrawords_size = 0;
  if (ctpFormatVersion>=2) {
    extrawords_size = CTPfragment::numberExtraPayloadWords(rbf);
  };
  int32_t nwords_trigger_per_bc(0);
  if (ctpFormatVersion<4) {
    nwords_trigger_per_bc = CTPdataformat::DAQwordsPerBunch_v0;
  } else if (ctpFormatVersion==4) {
    nwords_trigger_per_bc = CTPdataformat::DAQwordsPerBunch_v4;
  } else {
    if (is_l2) {
      nwords_trigger_per_bc = CTPdataformat::LVL2wordsPerBunch_v5;
    } else {
      nwords_trigger_per_bc = CTPdataformat::DAQwordsPerBunch_v4;
    }
  }
  int32_t readoutwindow=0;

  std::cout << std::dec;
  std::cout << "Fragment size  = " << fragment_size << " words = " << fragment_size*4 << " bytes" << std::endl;
  std::cout << "Header size    = " << header_size << " words = " << header_size*4 << " bytes" << std::endl;
  std::cout << "Trailer size   = " << trailer_size << " words = " << trailer_size*4 << " bytes" << std::endl;
  std::cout << "Status size    = " << status_size << " words = " << status_size*4 << " bytes" << std::endl;
  std::cout << "Payload size   = " << payload_size << " words = " << payload_size*4 << " bytes" << std::endl;

  uint32_t nwords_trigger = payload_size-timestampwords_size-extrawords_size;
  if ( ((nwords_trigger % nwords_trigger_per_bc) == 0)
       && nwords_trigger_per_bc != 0) {
    readoutwindow = nwords_trigger / nwords_trigger_per_bc;
    std::cout << "Size per BC    = " <<  nwords_trigger_per_bc << " words = " <<  nwords_trigger_per_bc << " bytes" << std::endl;
    std::cout << "Readout window = " << readoutwindow << " BC " << std::endl;
  } else {
    std::cout << "ERROR: inconsistent payload !!" << std::endl;
    std::cout << "ERROR: payload      = " << payload_size << " words" << std::endl;
    std::cout << "ERROR: extra words  = " << extrawords_size << " words" << std::endl;
    std::cout << "ERROR: timestamp    = " << timestampwords_size << " words" << std::endl;
    std::cout << "ERROR: number of trigger words " << nwords_trigger << " is inconsistent with trigger words per BC = " << nwords_trigger_per_bc << " words" << std::endl;
  } 

  // print header marker
  // std::right << std::setw(8) << std::setfill('0')
  printf("H HEAD  Header Marker | 0x%08x\n", headermarker);
  printf("H HSIZ    Header Size | 0x%08x\n", headersize);
  printf("H FMT  Format Version | 0x%08x\n", formatversionnumber);

  // decode "format version number"
  uint32_t rodversion=(formatversionnumber&0xffff0000)>>16;
  printf("H  |_    Event Format | 0x%04x\n", rodversion);
  uint32_t l1a = CTPfragment::lvl1AcceptBunch(rbf);
  if (ctpFormatVersion==0) {
    // do nothing
  } else if (ctpFormatVersion==1) {
    // print L1A position
    printf("H  |_    L1A Position | 0x%02x (%d)   (bits 13..8)\n", l1a, l1a);
  } else if (ctpFormatVersion>=2) {
    uint32_t numberExtraPayloadWords = CTPfragment::numberExtraPayloadWords(rbf);
    printf("H  |_     Extra words | 0x%02x (%d)   (bits 15..10)\n", numberExtraPayloadWords, numberExtraPayloadWords);
    printf("H  |_    L1A Position | 0x%02x (%d)   (bits 9..4)\n", l1a, l1a);
  }
  printf("H  |_     CTP version | %d\n", ctpFormatVersion);

  // print source identifier
  if(rbf->rod_source_id() == daq_id) {
    printf("H HDAQ      Source Id | 0x%08x  CTP DAQ fragment\n", sourceidentifier);
  } else if (rbf->rod_source_id() == lvl2_id) {
    printf("H HROI      Source Id | 0x%08x  CTP Level-2 ROI fragment\n", sourceidentifier);
  } else {
    printf("H           Source Id | 0x%08x  ERROR: unidentified source identifier\n", sourceidentifier);
  }
  
  uint32_t head_runno = rbf->rod_run_no();
  printf("H HRUN     Run Number | 0x%08x (%d)\n", head_runno, head_runno);
  uint32_t head_l1id = rbf->rod_lvl1_id();
  uint32_t l1id_ecrc = (head_l1id&0xff000000)>>24;
  uint32_t l1id_ec = head_l1id&0xffffff;
  printf("H L1ID     Level-1 ID | 0x%08x (%d, %d)\n", head_l1id, l1id_ecrc, l1id_ec);
  uint32_t head_bcid = rbf->rod_bc_id();
  printf("H BCID           BCID | 0x%03x (%d)\n", head_bcid, head_bcid);
  uint32_t head_tt = rbf->rod_lvl1_trigger_type();
  printf("H TTYP   Trigger Type | 0x%02x   ", head_tt);
  std::vector<eformat::helper::StreamTag> streamTags;
  uint8_t l1tt = static_cast<uint32_t>(head_tt);
  CTPfragment::streamTags(l1tt, streamTags);
  std::cout << std::showbase;
  std::cout << std::boolalpha;
  for (unsigned int i = 0; i<streamTags.size(); ++i) {
    std::cout << "StreamTag " << i << " - type '" << streamTags[i].type 
	      << "' name '" << streamTags[i].name << "' obeys LB '" 
	      << streamTags[i].obeys_lumiblock << "'";
  }
  std::cout << std::noboolalpha;
  std::cout << std::dec << std::noshowbase;
  std::cout << ")" << std::endl;

  // detector event type
  uint32_t detev_type = rbf->rod_detev_type();
  printf("H DETT Det Event Type | 0x%08x\n", detev_type);

  uint32_t hltCounter = CTPfragment::hltCounter(rbf);
  if (ctpFormatVersion==0) {
    printf("H  |_      HLTCounter | 0x%03x (%d)   (bits 31..22)\n", hltCounter, hltCounter);
    printf("H  |_    L1A Position | 0x%02x (%d)   (bits 21..16)\n", l1a, l1a);
  } else if (ctpFormatVersion>=1) {
    printf("H  |_      HLTCounter | 0x%04x (%d)   (bits 31..16)\n", hltCounter, hltCounter);
  }
  uint32_t lbn = CTPfragment::lumiBlockNumber(rbf);
  printf("H  |_       Lumiblock | 0x%04x (%d)     (bits 15..0)\n", lbn, lbn);
  
  std::cout << "----------------------+---------------------------------------------" << std::endl;
  uint32_t sec,nsec;
  CTPfragment::time(rbf,sec,nsec);
  printf("D TIME    Nanoseconds | 0x%08x (in units of 5ns) = %d ns\n", nsec/5, nsec);
  printf("D TIME        Seconds | 0x%08x (%d)\n", sec, sec);
  time_t tt = static_cast<time_t>(sec);
  std::string timestring(ctime(&tt),24);
  std::cout << "D  |_            Date | " << timestring << std::endl;
  std::cout << "D Trigger Bits        | of triggered bunch at position " << std::dec << l1a << " displayed first here." << std::endl;
  std::cout << "D                     | See below for complete readout window" << std::endl;
  printf("D Aux PIT Word        | 0x%08x\n", CTPfragment::patternInTimeAuxWord(rbf));

  uint16_t bcid = CTPfragment::bcid(rbf);
  if (ctpFormatVersion==0) {
    printf("D  |_     12-bit BCID | 0x%03x (%d) \n", bcid, bcid);
  } else if ((ctpFormatVersion>0) && (ctpFormatVersion<4)) {
    printf("D  |_      4-bit BCID | 0x%1x (%d)\n", bcid, bcid);
    uint16_t bginfo = CTPfragment::bunchGroup(rbf);
    printf("D  |_     Bunch Group | 0x%02x\n", bginfo);
  } else {
    printf("D  |_     12-bit BCID | 0x%03x (%d)\n", bcid, bcid);
    uint16_t bginfo = CTPfragment::bunchGroup(rbf);
    printf("D  |_     Bunch Group | 0x%02x\n", bginfo);
  }
  std::cout << std::boolalpha;
  std::vector<bool> rnd = CTPfragment::randomTriggers(rbf);
  for (size_t ir = 0; ir<rnd.size(); ++ir) {
    std::cout << "D  |_        Random " << ir << " | " << rnd[ir]  << std::endl;
  }
  std::vector<bool> psclock = CTPfragment::prescaledClockTriggers(rbf);
  for (size_t ip = 0; ip<psclock.size(); ++ip) {
    std::cout << "D  |_    psc. Clock "<< ip << " | " << psclock[ip]  << std::endl;
  }   
  std::vector<uint32_t> tav = CTPfragment::triggerDecision(rbf);
  std::vector<uint32_t> tap = CTPfragment::triggerDecisionAfterPrescales(rbf);
  std::vector<uint32_t> tbp = CTPfragment::triggerDecisionBeforePrescales(rbf);
  std::vector<uint32_t> pit = CTPfragment::patternInTimeTriggerInputs(rbf);
  std::vector<uint32_t>::reverse_iterator tav_it = tav.rbegin();
  std::vector<uint32_t>::reverse_iterator tap_it = tap.rbegin();
  std::vector<uint32_t>::reverse_iterator tbp_it = tbp.rbegin();
  std::vector<uint32_t>::reverse_iterator pit_it = pit.rbegin();
   
  //First just show information for L1A bunch
   
  //
  // PITs
  //
  // if (ctpFormatVersion<=3) {
  //   std::cout << "D PIT words 5..0      | 0x";
  //   ++pit_it; // skip last PIT word (rnd, ps. clock)
  //   for(; pit_it != pit.rend(); ++pit_it) {
  //     printf("%08x", (*pit_it));
  //     if(pit_it != (pit.rend()-1) ) printf(" | ");
  //   }
  // } else {

  unsigned int wordsperline(4); // number of words to be printed in one line

  if ( (ctpFormatVersion<=4) || ((ctpFormatVersion>4) && (!is_l2)) ) {
    unsigned npitwords(pit.size()-1);
    unsigned int hw(npitwords);
    ++pit_it; // skip last PIT word
    for(; pit_it != pit.rend(); ++pit_it) {
      if ((hw-(npitwords%wordsperline))%wordsperline==0) {
	if ((hw!=npitwords)&&(hw!=0)) printf("\n");
	printf("D PIT words %2u..%2u    | 0x", hw-1, (hw>wordsperline)? hw-wordsperline : 0);
      } 
      printf("%08x", (*pit_it));
      if(pit_it != (pit.rend()) ) printf(" | ");
      --hw;
    }
    printf("\n");

    pit_it = pit.rbegin();
    int nPITs = npits-1;
    ++pit_it; // skip last PIT word (rnd, ps. clock)
    for(; pit_it != pit.rend(); ++pit_it) {
      printf("D  |_      %03d to %03d | ", nPITs, nPITs-31);
      for (int32_t bit = 31; bit>=0;--bit,--nPITs) {
	( ((*pit_it)>>bit) & 1 ) ? printf("1") : printf("0");
	if(bit==0) printf("\n");
      }
    }
  }
   
  //
  // TBPs
  {
    unsigned ntbpwords(tbp.size());
    unsigned int hw = ntbpwords;
    for(; tbp_it != tbp.rend(); ++tbp_it) {
      if ((hw-(ntbpwords%wordsperline))%wordsperline==0) {
	if ((hw!=ntbpwords)&&(hw!=0)) printf("\n");
	printf("D TBP words %2u..%2u    | 0x", hw-1, (hw>wordsperline)? hw-wordsperline : 0);
      } 
      printf("%08x", (*tbp_it));
      if(tbp_it != (tbp.rend()) ) printf(" | ");
      --hw;
    }
    printf("\n");

    int nBITs = nitems-1;
    for(tbp_it = tbp.rbegin(); tbp_it != tbp.rend(); ++tbp_it) {
      printf("D  |_      %03d to %03d | ", nBITs, nBITs-31);
      for (int32_t bit = 31; bit>=0;--bit,--nBITs) {
	( ((*tbp_it)>>bit) & 1 ) ? printf("1") : printf("0");
	if(bit==0) printf("\n");
      } 
    }
  }

  //
  // TAPs
  //
  if ( (ctpFormatVersion<=4) || ((ctpFormatVersion>4) && (!is_l2)) ) {
    unsigned ntapwords(tap.size());
    unsigned int hw = ntapwords;
    for(; tap_it != tap.rend(); ++tap_it) {
      if ((hw-(ntapwords%wordsperline))%wordsperline==0) {
	if ((hw!=ntapwords)&&(hw!=0)) printf("\n");
	printf("D TAP words %2u..%2u    | 0x", hw-1, (hw>=wordsperline)? hw-wordsperline : 0);
      } 
      printf("%08x", (*tap_it));
      if(tap_it != (tap.rend()) ) printf(" | ");
      --hw;
    }
    printf("\n");
    int nBITs = nitems-1;
    for(tap_it = tap.rbegin(); tap_it != tap.rend(); ++tap_it) {
      printf("D  |_      %03d to %03d | ", nBITs, nBITs-31);
      for (int32_t bit = 31; bit>=0;--bit,--nBITs) {
	( ((*tap_it)>>bit) & 1 ) ? printf("1") : printf("0");
	if(bit==0) printf("\n");
      } 
    }
  }

  //
  // TAVs
  //
  {
    unsigned ntavwords(tav.size());
    unsigned int hw = ntavwords;
    for(; tav_it != tav.rend(); ++tav_it) {
      if ((hw-(ntavwords%wordsperline))%wordsperline==0) {
	if ((hw!=ntavwords)&&(hw!=0)) printf("\n");
	printf("D TAV words %2u..%2u    | 0x", hw-1, (hw>=wordsperline)? hw-wordsperline : 0);
      } 
      printf("%08x", (*tav_it));
      if(tav_it != (tav.rend()) ) printf(" | ");
      --hw;
    }
    printf("\n");
    int nBITs = nitems-1;
    for(tav_it = tav.rbegin(); tav_it != tav.rend(); ++tav_it) {
      printf("D  |_      %03d to %03d | ", nBITs, nBITs-31);
      for (int32_t bit = 31; bit>=0;--bit,--nBITs) {
	( ((*tav_it)>>bit) & 1 ) ? printf("1") : printf("0");
	if(bit==0) printf("\n");
      } 
    }
  }
   
  //now dump all the bits in a matrix
  if(!is_l2) {
    std::vector<std::vector<int>> pitBits(maxwindow, std::vector<int>(npits));
    // int pitBits[maxwindow][npits];
    std::vector<std::vector<int>> trigBits(maxwindow, std::vector<int>(nitems));
    // int trigBits[maxwindow][nitems];
    for (int i=0; i<maxwindow; i++) {
      for(int j=0; j<npits; j++) {pitBits[i][j]=0;}
      for(int j=0; j<nitems; j++) {trigBits[i][j]=0;}
    }
    unsigned int nBC=0;
    std::vector<uint32_t> bits;
    for(int bc=0; bc<readoutwindow; bc++) {
      bits = CTPfragment::patternInTimeTriggerInputs(rbf, bc);
      if(bits.size()>0) {
	nBC++;
	int nBITs=npits-1;
	for(int i=bits.size()-2; i>=0; i--) {//skip sixth PIT word
	  uint32_t word=bits.at(i);
	  for (int bit = 31; bit>=0; --bit) {
	    if( (word >> bit) & 1 ) {pitBits[bc][nBITs]=1;}
	    nBITs--;
	  }
	}
      }
      bits.clear();
      bits = CTPfragment::triggerDecisionBeforePrescales(rbf, bc);
      if(bits.size()>0) {
	int nBITs=nitems-1;
	for(int i=bits.size()-1; i>=0; i--) {
	  uint32_t word=bits.at(i);
	  for (int bit = 31; bit>=0; --bit) {
	    if( (word >> bit) & 1 ) {trigBits[bc][nBITs]=1;}
	    nBITs--;
	  }
	}
      }
      bits.clear();
      bits = CTPfragment::triggerDecisionAfterPrescales(rbf, bc);
      if(bits.size()>0) {
	int nBITs=nitems-1;
	for(int i=bits.size()-1; i>=0; i--) {
	  uint32_t word=bits.at(i);
	  for (int bit = 31; bit>=0; --bit) {
	    if( (word >> bit) & 1 ) {trigBits[bc][nBITs]=2;}
	    nBITs--;
	  }
	}
      }
      bits.clear();
      bits = CTPfragment::triggerDecision(rbf, bc);
      if(bits.size()>0) {
	int nBITs=nitems-1;
	for(int i=bits.size()-1; i>=0; i--) {
	  uint32_t word=bits.at(i);
	  for (int bit = 31; bit>=0; --bit) {
	    if( (word >> bit) & 1 ) {trigBits[bc][nBITs]=3;}
	    nBITs--;
	  }
	}
      }
      bits.clear();
    }
    std::cout << std::endl;
    std::cout << "      PIT Bits Per BC" << std::endl;
    std::cout << "      Legend: \'1\' = trigger" << std::endl;
    std::cout << "              \'-\' = no trigger" << std::endl;
    std::cout << "              \'+\' = no trigger at L1A position" << std::endl;
    std::cout << "     ";
    for(unsigned int y=0; y<nBC; y++) {
      if(y%10==0) std::cout << " ";
      if(y==l1a) std::cout << "A";
      else std::cout << " ";} 
    std::cout << std::endl << "   B ";
    for(unsigned int y=0; y<nBC; y++) {if(y%10==0) std::cout << " "; int tens = y/10; std::cout << tens;} 
    std::cout << std::endl << "   C ";
    for(unsigned int y=0; y<nBC; y++) {if(y%10==0) std::cout << " "; int ones = y%10; std::cout << ones;} 
    std::cout << std::endl << std::endl;
   
    for(int i=0; i<npits; i++) {

      int y=0;
      for(unsigned int j=0; j<nBC; j++) {
	y+=pitBits[j][i];
      }
      if(y!=0) {
	std::cout << std::setw(4) << std::right << std::setfill(' ') << i << " ";
	for(unsigned int j=0; j<nBC; j++) {
	  if(j%10==0) std::cout << " "; 
	  if(pitBits[j][i]!=0) std::cout << std::setw(1) << pitBits[j][i];
	  else if(j==l1a) std::cout << "+";
	  else std::cout << "-";
	}
	std::cout << std::endl;
      }
    }
    std::cout << std::endl;
    std::cout << "      Trigger Bits Per BC" << std::endl;
    std::cout << "      Legend: \'-\' = no trigger" << std::endl;
    std::cout << "              \'1\' = TBP only" << std::endl;
    std::cout << "              \'2\' = TAP&TBP only" << std::endl;
    std::cout << "              \'3\' = TAV&TAP&TBP" << std::endl;
    std::cout << "              \'+\' = no trigger at L1A position" << std::endl;
    std::cout << "     ";
    for(unsigned int y=0; y<nBC; y++) {
      if(y%10==0) std::cout << " ";
      if(y==l1a) std::cout << "A";
      else std::cout << " ";} 
    std::cout << std::endl << "   B ";
    for(unsigned int y=0; y<nBC; y++) {if(y%10==0) std::cout << " "; int tens = y/10; std::cout << tens;} 
    std::cout << std::endl << "   C ";
    for(unsigned int y=0; y<nBC; y++) {if(y%10==0) std::cout << " "; int ones = y%10; std::cout << ones;} 
    std::cout << std::endl << std::endl;
   
    for(int i=0; i<nitems; i++) {
      int y=0;
      for(unsigned int j=0; j<nBC; j++) {
	y+=trigBits[j][i];
      }
      if(y!=0) {
	std::cout << std::setw(4) << std::right << std::setfill(' ') << i << " ";
	for(unsigned int j=0; j<nBC; j++) {
	  if(j%10==0) std::cout << " "; 
	  if(trigBits[j][i]!=0) std::cout << std::setw(1) << trigBits[j][i];
	  else if(j==l1a) std::cout << "+";
	  else std::cout << "-";
	}
	std::cout << std::endl;
      }
    }

   
    //Now compare all words
    if(allBunches && !is_l2) {
   
      std::cout << std::endl;
   
      bool fired[3];

      for(unsigned int bc=0; bc<nBC; bc++) {
	int nBITs=nitems-1;
	tav = CTPfragment::triggerDecision(rbf,bc);
	tap = CTPfragment::triggerDecisionAfterPrescales(rbf,bc);
	tbp = CTPfragment::triggerDecisionBeforePrescales(rbf,bc);
	tav_it = tav.rbegin();
	tap_it = tap.rbegin();
	tbp_it = tbp.rbegin();
         
	if(tbp.size() != tap.size() || tbp.size() != tav.size()) {
	  std::cout << "TBP/TAP/TAV vectors have different number of entries!" << std::endl;
	  continue;
	}
         
	for(; tbp_it != tbp.rend(); ++tbp_it,++tap_it,++tav_it) {
	  std::cout << std::setw(3) << std::dec;
	  for (int32_t bit = 31; bit>=0;--bit,--nBITs) {
	    fired[0]=(((*tbp_it)>>bit) & 1);
	    fired[1]=(((*tap_it)>>bit) & 1);
	    fired[2]=(((*tav_it)>>bit) & 1);
      
	    if(fired[0] || fired[1] || fired[2]) {
	      if(bc!=l1a) std::cout << "  BC " << std::setw(2) << bc << " Item " << std::setw(3) << nBITs << " ";
	      else std::cout << "> BC " << std::setw(2) << bc << " Item " << std::setw(3) << nBITs << " ";
	      if(fired[0]) std::cout << "TBP "; else std::cout << "    "; 
	      if(fired[1]) std::cout << "TAP "; else std::cout << "    "; 
	      if(fired[2]) std::cout << "TAV ";
	      std::cout << std::endl;
	    }
	  }
	}
      }
    }

  }

  // Additional words (for version >=2)
  if (ctpFormatVersion>=2) {
    std::vector<uint32_t> additionalwords = CTPfragment::extraPayloadWords(rbf);
    size_t additionalwordssize=additionalwords.size();
    printf("D extra payload words | %d\n", static_cast<int>(CTPfragment::numberExtraPayloadWords(rbf)));
    printf("D additional words    | %d\n", static_cast<int>(additionalwordssize));

    // time difference to previous trigger (for version>=2)
    if (additionalwordssize>0) {
      uint32_t timeSincePreviousL1A = CTPfragment::bunchCrossingsSincePreviousL1A(rbf);
      printf("D  |_       time diff | 0x%08x (%d) BC\n", timeSincePreviousL1A, timeSincePreviousL1A);
    }
    // turn counter (for version >=3)
    if ((additionalwordssize>1) && (ctpFormatVersion>=3)) {
      uint32_t turnCounter = CTPfragment::turnCounter(rbf);
      printf("D  |_    turn counter | 0x%08x (%d) turns\n", turnCounter, turnCounter); 
    }

    for (size_t i = 0; i<additionalwords.size(); ++i) {
      if ((i==0) && (ctpFormatVersion>=4)) {
	uint32_t bgk = 0xffff&(additionalwords[i]>>16);
	uint32_t l1psk = 0xffff&additionalwords[i];
	printf("D  |_    add. word %02d | 0x%08x (BGK=%d, L1PSK=%d)\n", static_cast<int>(i), additionalwords[i], bgk, l1psk);
      } else {
	printf("D  |_    add. word %02d | 0x%08x\n", static_cast<int>(i), additionalwords[i]);
      }
    }	
  }

  // number of status elements
  uint32_t rod_nstatus = rbf->rod_nstatus();
  std::string nstatus_check = (rod_nstatus==2? "OK" : "ERROR");
  if (rod_nstatus==2) {
    const uint32_t* rod_status_words = rbf->rod_status();
    uint32_t status_error = rod_status_words[0];
    uint32_t status_info  = rod_status_words[1];
    std::string status_error_check = (status_error==0? "OK" : "ERROR");
    std::string status_info_check  = (status_info==0? "OK" : "ERROR");
    std::cout << "S SERR                | " << std::hex << status_error << " (has to be 0: " << status_error_check << ")" << std::endl;
    std::cout << "S SNFO                | " << std::hex << status_info << " (has to be 0: " << status_info_check << ")" << std::endl;
  } 

  // number of status elements: print out at the right position
  std::cout << "T SNUM # status words | " << std::dec << rod_nstatus << " (has to be 2: "<< nstatus_check << ")" << std::endl;

  // number of data elements
  uint32_t rod_ndata = rbf->rod_ndata();
  if(rbf->rod_source_id() == daq_id) {
    printf("T SDAQ # data words   | %d\n", rod_ndata);
  }
  else if(rbf->rod_source_id() == lvl2_id) {
    printf("T SROI # data words   | %d\n", rod_ndata);
  }

  // Position of status elements
  uint32_t rod_status_position = rbf->rod_status_position();
  std::string status_position_check = (rod_status_position==1? "OK" : "ERROR");
  printf("T SPOS status position| %d (has to be 1: %s)\n", rod_status_position, status_position_check.c_str());
}
   
/**
 * Dump one event into a file. To be used for corrupted events.
 */
void dumpDataBlock(unsigned int counter,char* pt,unsigned int sizeBytes)
{
  std::cerr << "Event " << counter << " is corrupted and will be written at a separate file." 
	    << std::endl << std::flush; 
  std::ostringstream n;
  n << "dump_corrupted_event_" << counter << ".dat";
  std::string name = n.str();
  std::fstream out(name.c_str(), std::ios::out|std::ios::binary);
  out.write(pt,sizeBytes);
  out.close();
  std::cerr << "Event dumped to " << name << std::endl << std::flush; 
}

int main (int argc, char *argv[])
{
  using namespace eformat;
   
  // parse command line parameters
  boost::program_options::options_description 
    desc("Text dumper for CTP/MuCTPI raw data fragments");
  desc.add_options()
    ("help,h", "Help Message")
    ("ctpOnly,c", "Suppress MuCTPI Output")
    ("daqOnly,d", "Suppress LVL2 Fragment Info")
    ("allBunches,a", "Fully Decode All Bunches")
    ("startEvent,s", boost::program_options::value<int>(), "First Event to Skim (Default 1)")
    ("nEvents,n", boost::program_options::value<int>(), "Number of Events to Skim (Default -1=ALL)")
    ("filename,f",  boost::program_options::value<std::string>(), "Name of file to dump")
    ;
   
  boost::program_options::positional_options_description p;
  p.add("filename", -1);
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  boost::program_options::notify(vm);
   
  if(vm.count("help")) {
    std::cout << desc << std::endl;
    return EXIT_SUCCESS;
  }
  if(vm.count("ctpOnly")) {
    ctpOnly=true;
  }
  if(vm.count("daqOnly")) {
    daqOnly=true;
  }
  if(vm.count("allBunches")) {
    allBunches=true;
  }
  if(vm.count("startEvent")) {
    lowEvent = vm["startEvent"].as<int>();
  }
  if(vm.count("nEvents")) {
    nEvents = vm["nEvents"].as<int>();
  }
  if(vm.count("filename")) {
    filename = vm["filename"].as<std::string>();
  }
  else {
    std::cout << "No filename specified!" << std::endl;
    std::cout << desc << std::endl;
    return EXIT_FAILURE;
  }
   
  DataReader *pDR = pickDataReader( static_cast<std::string>(filename) );
  if(!pDR) {
    std::cout << "Problem opening or reading this file!\n";
    return -1;
  }
  if(!pDR->good()) {
    std::cout << "There is a problem!" << std::endl;
    return -1;
  }
  // This info (for instance) is available without penalty at start time.
   
  int eventCounter=0;
  int robCounter=0;
  unsigned int invalidEvents=0;
   
  int stopEvent = -1;
  if(nEvents >= 0) stopEvent=lowEvent+nEvents;
   
  while(pDR->good()) 
    {
      unsigned int eventSize;    
      char *buf;
      
      DRError ecode = pDR->getData(eventSize,&buf);
      if(DROK != ecode) {
	std::cout << "Can't read from file!" << std::endl;
	break;
      }
      ++eventCounter;
      if(lowEvent > eventCounter) continue;
      
      if(stopEvent>0 && eventCounter>=stopEvent) {
	break;
      }
      
      // now start the eformat business
      uint32_t* fragment = reinterpret_cast<uint32_t*>(buf);
      bool isROD = false;
      try {
	switch ((eformat::HeaderMarker)(fragment[0])) 
	  {
	  case FULL_EVENT:
	    {
	      FullEventFragment<const uint32_t*> fe(fragment);
	      fe.check();
	      break;
	    }
	  case ROD:
	    {
	      std::cout << "Reading ROD fragment(s) with event size " << eventSize << " bytes" << std::endl;
	      isROD = true;
	      break;
	    }
	  default:
            std::cerr << "Fragment type not recognized after conversion, skip.";
            ++invalidEvents;
            dumpDataBlock(eventCounter,buf,eventSize);
            delete [] buf;
            continue;
	  }
         
	// now that we have a valid fragment, let us unpack down to ROBs
	// const uint32_t* robs[MAX_ROB_COUNT];
	// size_t Nrob = get_robs (fragment,robs,MAX_ROB_COUNT);
	std::vector<std::shared_ptr<const uint32_t>> robs;

	if (isROD) {
	  robs.resize(1);
	} else {
	  get_robs(fragment, robs);
	}

	const uint32_t mdaq_id = (eformat::TDAQ_MUON_CTP_INTERFACE << 16) | 0x0;
	const uint32_t cdaq_id = (eformat::TDAQ_CTP << 16) | 0x0;

	// for (size_t irob=0; irob<Nrob; irob++) {
	for (size_t irob=0; irob<robs.size(); irob++) {
	  // ROBFragment<const uint32_t*> *rbf = new ROBFragment<const uint32_t*>(robs[irob]);
	  //	  ROBFragment<const uint32_t*> *rbf = new ROBFragment<const uint32_t*>(reinterpret_cast<const uint32_t*>(robs[irob]));
	  eformat::ROBFragment<const uint32_t*> *rbf = 0;
	  uint32_t* buffer =0;
	  if (isROD) {
	    eformat::write::ROBFragment arob( fragment, eventSize/sizeof(uint32_t));
	    const eformat::write::node_t* first = arob.bind();
	    uint32_t size = eformat::write::count_words(*first);
	    auto buffer = new uint32_t[size];
	    eformat::write::copy(*first, buffer, size);
	    rbf = new eformat::ROBFragment<const uint32_t*>(buffer);
	  } else {
	    std::shared_ptr<const uint32_t> arob = robs[irob];
	    const uint32_t* arob_p = &(*arob);
	    rbf = new ROBFragment<const uint32_t*>(arob_p);
	  }
	  ++robCounter;
	  if( (rbf->rod_source_id()>>16) == eformat::TDAQ_CTP ) {
	    // CTP fragment
	    if(daqOnly==false || rbf->rod_source_id() == cdaq_id) dumpCtp(rbf);
	  }
	  else if( ctpOnly==false && (rbf->rod_source_id()>>16) == eformat::TDAQ_MUON_CTP_INTERFACE ){
	    // MuCTPI fragment
	    if(daqOnly==false || rbf->rod_source_id() == mdaq_id) dumpMuctpi(rbf);
	  }
	  delete rbf;
	  if (isROD) delete buffer;
	}
      } catch (eformat::Issue& ex) {
	std::cerr << "Uncaught eformat issue: " << ex.what() << std::endl;
	dumpDataBlock(eventCounter,buf,eventSize);
	delete [] buf;
	continue;
      } catch (ers::Issue& ex) {
	std::cerr << "Uncaught ERS issue: " << ex.what() << std::endl;
	dumpDataBlock(eventCounter,buf,eventSize);
	delete [] buf;
	continue;
      } catch (std::exception& ex) {
	std::cerr << "Uncaught std exception: " << ex.what() << std::endl;
	dumpDataBlock(eventCounter,buf,eventSize);
	delete [] buf;
	continue;
      } catch (...) {
	std::cerr << std::endl << "Uncaught unknown exception" << std::endl;
	dumpDataBlock(eventCounter,buf,eventSize);
	delete [] buf;
	continue;
      }
      // end of proccedding for this data block
      delete [] buf;
    }
  std::cout << std::endl;	

  // 	std::cout << "Retrived " << eventCounter << " events.\n";
  // 	std::cout << "Found " << robCounter << " ROB fragments.\n";
  // 	std::cout << "Last one has number " << pDR->latestDataNumber() << "." << std::endl;
   
  // 	std::cout << "Events in run " << pDR->eventsInFileSequence() << std::endl;
  // 	std::cout << "MB in run " << pDR->dataMB_InFileSequence() << std::endl;
  //     std::cout << "Run end time " << pDR->runEndTime() << std::endl;
  //     std::cout << "Run end date " << pDR->runEndDate() << std::endl;
  delete pDR;
}
